<?php
/**
 * @package Linxact
 */
/*
Plugin Name: Yotpo Custom Settings
Description: Yotpo Custom Settings Plugin
Version: 1.0.0
Author: apollon.guru
Author URI: https://apollon.guru/
License: GPLv2 or later
*/

require_once 'inc/main.php';


add_action('admin_enqueue_scripts', function(){
    // Localize the script with new data
    $translation_array = array(
        'ajaxUrl' => admin_url( 'admin-ajax.php' )
    );

    wp_localize_script( 'lin_global', 'globalVar', $translation_array );
    wp_enqueue_script( 'lin_global' );

    wp_register_script( 'yotpo_custom', plugin_dir_url(__FILE__) . 'assets/js/yotpo_custom.js',  array('jquery'), '0.0.1', true );
    wp_localize_script( 'yotpo_custom-search', 'wp', array('ajaxUrl' => admin_url( 'admin-ajax.php' )));
    wp_register_style( 'yotpo_custom', plugin_dir_url(__FILE__) . '/assets/css/yotpo_custom.css', array( ), '0.0.1');
});

wp_register_style('settingscss', plugin_dir_url(__FILE__) . 'assets/css/yotpo_custom__front.css', array( ), '20180920');
wp_enqueue_style( 'settingscss');

//wp_register_style('slick_settingscss', plugin_dir_url(__FILE__) . 'assets/slick_slider/slick.css', array( ), '4.0.2');
//wp_enqueue_style( 'slick_settingscss');

add_action( 'wp_enqueue_scripts', function () {
    wp_register_style('owlcaruselcss', plugin_dir_url(__FILE__) . 'assets/css/owl.carusel.css', array( ), '');
    wp_enqueue_style( 'owlcaruselcss');

    //сликслайдер js
    wp_register_script('slick_carousel', plugin_dir_url(__FILE__) . 'assets/js/owl.carousel.js', array('jquery'), array());
    wp_enqueue_script('slick_carousel');
}, '20' );

//сликслайдер js
//wp_register_script('slick_sliderjs1', plugin_dir_url(__FILE__) . 'assets/slick_slider/slick.js', array('jquery'), '5.9.4');
//wp_enqueue_script( 'slick_sliderjs1');

wp_register_script('settingjss1','https://use.fontawesome.com/9ea45ce4ff.js', array('jquery'));
wp_enqueue_script( 'settingjss1');

wp_register_script('settingjss2', plugin_dir_url(__FILE__) . 'assets/js/yotpo_custom__front.js', array('jquery'), '20180922');
wp_enqueue_script( 'settingjss2');
//аякс
wp_register_script('settiserch', 'wp', array('ajaxUrl' => admin_url( 'admin-ajax.php' )));
wp_enqueue_script( 'settiserch');

add_action('wp_head', 'myplugin_ajaxurl');

function myplugin_ajaxurl() {
    echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}
add_action('wp_enqueue_scripts', function(){
    wp_register_script( 'settingjss', plugin_dir_url(__FILE__) . '/assets/js/linxact_front.js',  array(), '2.5.0', true );
    wp_register_script('settiserch', 'wp', array('ajaxUrl' => admin_url( 'admin-ajax.php' )));
});

add_action('admin_enqueue_scripts', function(){
    wp_localize_script( 'userjs', 'wp', array('ajaxUrl' => admin_url( 'admin-ajax.php' )));
    wp_localize_script( 'settingsjs', 'wp', array('ajaxUrl' => admin_url( 'admin-ajax.php' )));
});

add_action('admin_menu', function(){
    add_menu_page('Yotpo Custom Settings', 'Yotpo Custom Settings', 'manage_options', 'yotpo_custom-settings', '', plugin_dir_url(__FILE__) . '/assets/img/linxact-logo-20px.png');
    add_submenu_page(  'yotpo_custom', 'Settings', 'Settings', 'manage_options', 'yotpo_custom-settings', 'page_settings_linxact');

});

//создание таблицы в базе данных при активации плагинав этой таблице будут храниться данные про отзывы.

global $jal_db_version;
$jal_db_version = '1.0';

function jal_install() {
    global $wpdb;
    global $jal_db_version;

    $table_name = $wpdb->prefix . 'yotpo_custom';
    $table_name_category = $wpdb->prefix . 'yotpo_custom_category';
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,

		created_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		updated_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		title text NOT NULL,
		product_name text NOT NULL,
		content text NOT NULL,
		score INT NOT NULL,
		sku INT NOT NULL,
		pid INT NOT NULL,
		name text NOT NULL,
		email text NOT NULL,
		cur_term text NOT NULL,
		like_count INT NOT NULL,
		dislike_count INT NOT NULL,
		id_review_api INT NOT NULL,
		PRIMARY KEY  (id),
        KEY `sku` (`sku`),
        KEY `pid` (`pid`),
        KEY `score` (`score`)
	) $charset_collate;";

    $sql_category = "CREATE TABLE $table_name_category (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		created_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		title text NOT NULL,
		category_name text NOT NULL,
        category_id mediumint(8) unsigned NOT NULL,
		content text NOT NULL,
		score INT NOT NULL,
		name text NOT NULL,

		like_count INT NOT NULL,
		dislike_count INT NOT NULL,

		PRIMARY KEY  (id),
        KEY category_id (category_id)
	) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    dbDelta( $sql_category );

    add_option( 'jal_db_version', $jal_db_version );
    add_option( 'home_page_star_rating', '5');
    add_option( 'home_page_star__count_rating', '1');
    add_option( 'home_page_star__center_all_rating', '5');
    add_option( 'yotpo_plugin_appkey', '');
    add_option( 'yotpo_plugin_secretkey', '');
    add_option( 'yotpo_plugin_time_update_count', '');
}

register_activation_hook( __FILE__, 'jal_install' );

function deactivation_yotpo_custom(){
    wp_clear_scheduled_hook('vbb_yp_get_reviews_hook');

    delete_option('yotpo_plugin_appkey');
    delete_option('yotpo_plugin_secretkey');
    delete_option('yotpo_plugin_time_update_count');
    delete_option("jal_db_version");
    delete_option( 'total_reviews');
    delete_option( 'average_score');

    global $wpdb;

    $wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}yotpo_custom" );
    $wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}yotpo_custom_category" );
}

register_deactivation_hook(__FILE__, 'deactivation_yotpo_custom');

//добавить событие на wp-cron
add_filter( 'cron_schedules', 'vbb_yp_cron_schedules_reviews' );
function vbb_yp_cron_schedules_reviews( $schedules ) {
    $cron_hours = (int) get_option('yotpo_plugin_time_update_count', 0);
    if($cron_hours){
        $schedules['vbb_yp_get_reviews'] = array(
            'interval' => HOUR_IN_SECONDS * $cron_hours,
            'display' => 'Save YotPo reviews'
        );

    }
    return $schedules;
}

// регистрируем событие
add_action('wp', 'vbb_yp_cron_add_hook');
function vbb_yp_cron_add_hook() {
    wp_clear_scheduled_hook('vbb_yp_get_reviews_hook');
    $cron_hours = (int) get_option('yotpo_plugin_time_update_count', 0);
    if($cron_hours){
        wp_schedule_event( time(), 'vbb_yp_get_reviews', 'vbb_yp_get_reviews_hook');
    }
}
// добавляем функцию к указанному хуку
add_action('vbb_yp_get_reviews_hook', 'vbb_yp_get_get_reviews_task');
function vbb_yp_get_get_reviews_task() {
    all_yotpo_reviews();
    visible_block_fk();
}

function visible_block_fk(){
    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom';
    $total_reviews = get_option('total_reviews');
    $total_reviews_for_func = ceil($total_reviews / 100);

    $yp_appkey = get_option('yotpo_plugin_appkey');
    $yp_secretkey = get_option('yotpo_plugin_secretkey');

    if(!$yp_appkey || !$yp_secretkey) return;

    for($i=1; $i<=$total_reviews_for_func; $i++){
        $curl = curl_init();

        $curlopt_url = "https://api.yotpo.com/v1/apps/{$yp_appkey}/reviews?utoken=7EODPn2zuXCC7prX6kM2J9efOzt5JFN3xjwrZdlL&page={$i}&count=100";
        $curlopt_postfields = "{\n  \"client_id\": \"{$yp_appkey}\",\n  \"client_secret\": \"{$yp_secretkey}\",\n  \"grant_type\": \"client_credentials\"\n}";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $curlopt_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => $curlopt_postfields,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: fe494f2f-bde8-c5e8-c01a-bacaad252296"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $all_reviews =  json_decode($response) ;

            $id_reviews_str = '';

            foreach( $all_reviews->reviews as $review ) {
                if( $id_reviews_str !== '' ) {
                    $id_reviews_str .= ',';
                }
                $id_reviews_str .= $review->id;
            }

            $existingReviewsObj = $wpdb->get_results("SELECT * FROM $name_db WHERE pid IN ($id_reviews_str)");
            $existingReviews = array();
            foreach ( $existingReviewsObj as $existingReview ) {
                $existingReviews[] = $existingReview->pid;
            }

            foreach( $all_reviews->reviews as $review ){

                if( !in_array( $review->id, $existingReviews ) ) {
                    $cur_terms = get_the_terms($review->sku, 'product_cat');
                    $product = wc_get_product($review->sku);

                    if ($product) {
                        $product_get_name_wc = $product->get_name();
                        $cur_term_category = '';
                        foreach ($cur_terms as $cur_term) {
                            $cur_term_category .= $cur_term->name . ",";
                        }

                        $wpdb->insert($name_db, array(
                            'id' => NULL,
                            'product_name' => $product_get_name_wc,
                            'sku' => $review->sku,
                            'pid' => $review->id,
                            'title' => $review->title,
                            'content' => $review->content,
                            'score' => $review->score,
                            'created_at' => $review->created_at,
                            'updated_at' => $review->updated_at,
                            'name' => $review->name,
                            'email' => $review->email,
                            'cur_term' => $cur_term_category,
                            'id_review_api' => $review->id,
                            'like_count' => $review->votes_up,
                            'dislike_count' => $review->votes_down
                        ));
                    }
                }
            }
        }
    }
}


// Добавляем количество общее отзывов
function all_yotpo_reviews() {

    $yp_appkey = get_option('yotpo_plugin_appkey');
    $yp_secretkey = get_option('yotpo_plugin_secretkey');
    $curlopt_url = "https://api.yotpo.com/products/{$yp_appkey}/yotpo_global_reviews/reviews";
    $curlopt_postfields = "{\n  \"client_id\": \"{$yp_appkey}\",\n  \"client_secret\": \"{$yp_secretkey}\",\n  \"grant_type\": \"client_credentials\"\n}";

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $curlopt_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => $curlopt_postfields,
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: 6137f80e-f1f3-4e80-3da3-9c89292bc965"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        $all_reviews =  json_decode($response) ;

        $total_reviews = $all_reviews->response->bottomline->total_reviews;
        $average_score = $all_reviews->response->bottomline->average_score;

        update_option( 'total_reviews', $total_reviews);
        update_option( 'average_score', $average_score);
    }
}

function all_yotpo_reviews_iik()
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://staticw2.yotpo.com/batch",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => " \n{\n  \"methods\": [{\n      \"method\":\"main_widget\",\n      \"params\": {\n        \"pid\": \"5820211\",\n         \"pid\": \"5820212\"\n       }\n  } \n \n],\n\"app_key\":\"t1FttIKKL8HHGXcSdrhA3Y5VmKc0K2Mq252un5ZO\"\n}",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: 7975797d-d7f8-3e36-394a-8c2bf90a0d17"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        $all_reviews = json_decode($response);
        print_r($all_reviews );
    }
}

function getpermalink_byid( $item )
{
    $url = get_permalink( $item );

    return $url;
}
function my_get_the_product_thumbnail_url($postid) {
    global $post;
    $size = 'shop_catalog';
    $image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );
    return get_the_post_thumbnail_url( $postid, $image_size );
}

function bars_rating_func_yotpo( $item ){
    $result_stars = '';
    if($item == '5'){
        $result_stars  = '
            <div class="yotpo-product-related-fields-bars">
                <div class="yotpo-field-bars-container">
                    <div class="yotpo-rating-bars ">
                        <div class="yotpo-product-related-field-score-bar yotpo-rating-bar-full"></div>
                        <div class="yotpo-product-related-field-score-divider"></div>
                        <div class="yotpo-product-related-field-score-bar yotpo-rating-bar-full"></div>
                        <div class="yotpo-product-related-field-score-divider"></div>
                        <div class="yotpo-product-related-field-score-bar yotpo-rating-bar-full"></div>
                        <div class="yotpo-product-related-field-score-divider"></div>
                        <div class="yotpo-product-related-field-score-bar yotpo-rating-bar-full"></div>
                        <div class="yotpo-product-related-field-score-divider"></div>
                        <div class="yotpo-product-related-field-score-bar yotpo-rating-bar-full"></div>
                        <div class="yotpo-product-related-field-score-divider"></div>
                        <div class="yotpo-clr"></div>
                    </div>
                </div>
            </div>
        ';
    }
    return $result_stars;
}

function stars_rating_func_yotpo( $item ){

    $result_stars = '';
    if($item == '1'){
        $result_stars  = '
                <div id="example_stars">
                    <span class="yotpo-icon yotpo-icon-star pull-left"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="2"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="3"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="4"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="5"></span>
                </div>
            ';
    }
    else if($item == '2'){
        $result_stars  = '
                <div id="example_stars">
                    <span class="yotpo-icon yotpo-icon-star pull-left" attr="1"></span>
                    <span class="yotpo-icon yotpo-icon-star pull-left" attr="2"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="3"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="4"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="5"></span>
                </div>
            ';
    }
    else if($item == '3'){
        $result_stars  = '
                <div id="example_stars">
                    <span class="yotpo-icon yotpo-icon-star pull-left" attr="1"></span>
                    <span class="yotpo-icon yotpo-icon-star pull-left" attr="2"></span>
                    <span class="yotpo-icon yotpo-icon-star pull-left" attr="3"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="4"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="5"></span>
                </div>
            ';
    }

    else if($item == '4'){
        $result_stars  = '
                <div id="example_stars">
                    <span class="yotpo-icon yotpo-icon-star pull-left" attr="1"></span>
                    <span class="yotpo-icon yotpo-icon-star pull-left" attr="2"></span>
                    <span class="yotpo-icon yotpo-icon-star pull-left" attr="3"></span>
                    <span class="yotpo-icon yotpo-icon-star pull-left" attr="4"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left"></span>
                </div>
            ';
    }
    else if($item == '5'){
        $result_stars  = '
                <div id="example_stars">
                    <span class="yotpo-icon yotpo-icon-star pull-left" attr="1"></span>
                    <span class="yotpo-icon yotpo-icon-star pull-left" attr="2"></span>
                    <span class="yotpo-icon yotpo-icon-star pull-left" attr="3"></span>
                    <span class="yotpo-icon yotpo-icon-star pull-left" attr="4"></span>
                   <span class="yotpo-icon yotpo-icon-star pull-left" attr="5"></span>
                </div>
            ';
    } else {
        $result_stars  = '
                <div id="example_stars">
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="1"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="2"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="3"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="4"></span>
                    <span class="yotpo-icon yotpo-icon-empty-star pull-left" attr="5"></span>
                </div>
            ';
    }
    return $result_stars;
}


function stars_rating_func( $item ){

    $result_stars = '';
    if($item == '1'){
        $result_stars  = '
                <div id="example_stars">
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs1" attr="1"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs2" attr="2"><i class="fas fa-star"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs3" attr="3"><i class="fas fa-star"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs4" attr="4"><i class="fas fa-star"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs5" attr="5"><i class="fas fa-star"></i></span>
                </div>
            ';
    }
    else if($item == '2'){
        $result_stars  = '
                <div id="example_stars">
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs1" attr="1"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs2" attr="2"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs3" attr="3"><i class="fas fa-star"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs4" attr="4"><i class="fas fa-star"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs5" attr="5"><i class="fas fa-star"></i></span>
                </div>
            ';
    }
    else if($item == '3'){
        $result_stars  = '
                <div id="example_stars">
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs1" attr="1"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs2" attr="2"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs3" attr="3"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs5" attr="5"><i class="fas fa-star"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs5" attr="5"><i class="fas fa-star"></i></span>
                </div>
            ';
    }

    else if($item == '4'){
        $result_stars  = '
                <div id="example_stars">
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs1" attr="1"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs2" attr="2"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs3" attr="3"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs4" attr="4"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs5" attr="5"><i class="fas fa-star"></i></span>
                </div>
            ';
    }
    else if($item == '5'){
        $result_stars  = '
                <div id="example_stars">
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs1" attr="1"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs2" attr="2"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs3" attr="3"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs4" attr="4"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                    <span class="star_rating_yotpo_custom star_rating_yotpo__cs5" attr="5"><i class="fas fa-star" style="color: #ffd200;"></i></span>
                </div>';
    }
    return $result_stars;
}

add_action( 'wp_ajax_ap_api_reg_key', 'ap_api_reg_key' );
add_action( 'wp_ajax_nopriv_ap_api_reg_key', 'ap_api_reg_key' );
function ap_api_reg_key()
{
    $plugin_appkey = trim($_POST['val_key']);

    update_option( 'yotpo_plugin_appkey', $plugin_appkey);
}
add_action( 'wp_ajax_ap_api_reg_secretkey', 'ap_api_reg_secretkey' );
add_action( 'wp_ajax_nopriv_ap_api_reg_secretkey', 'ap_api_reg_secretkey' );
function ap_api_reg_secretkey()
{
    $plugin_secretkey = trim($_POST['yp_secretkey']);
    update_option( 'yotpo_plugin_secretkey', $plugin_secretkey);
}
add_action( 'wp_ajax_ap_api_reg_time_cron', 'ap_api_reg_time_cron' );
add_action( 'wp_ajax_nopriv_ap_api_reg_time_cron', 'ap_api_reg_time_cron' );
function ap_api_reg_time_cron()
{
    // echo '<pre>$_POST = '.htmlspecialchars(print_r($_POST, true)).'</pre>';
    $cron_time = trim( $_POST['cron_time'] );
    update_option( 'yotpo_plugin_time_update_count', $cron_time);
}

//регистрация шорткода для вывода на фронтенд
function Vardenafil_func(){
    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom';
    $result ='';
    $newtable = $wpdb->get_results( "SELECT * FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%vardenafil%' or '%Super Zhewitra%' or '%Dapoxetine%' or '%Priligy 60 mg%' or '%Levitra%'  or '%Levitra 10 mg%' or '%Levitra 20 mg (Bestseller)%' or '%Levitra 40 mg%' or '%Levitra 60 mg%'  or '%Levitra Soft%' or '%Alternatives to Levitra%'   ORDER BY `wp_yotpo_custom`.`updated_at` DESC LIMIT 30" );

    $count_rew = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%vardenafil%'");

    $count_rew_score5 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%vardenafil%' and `score` = 5");
    $count_rew_score4 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%vardenafil%' and `score` = 4");
    $count_rew_score3 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%vardenafil%' and `score` = 3");
    $count_rew_score2 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%vardenafil%' and `score` = 2");
    $count_rew_score1 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%vardenafil%' and `score` = 1");

    $count_rew_percent5 = $count_rew_score5 / $count_rew * 100;
    $count_rew_percent4 = $count_rew_score4 / $count_rew * 100;
    $count_rew_percent3 = $count_rew_score3 / $count_rew * 100;
    $count_rew_percent2 = $count_rew_score2 / $count_rew * 100;
    $count_rew_percent1 = $count_rew_score1 / $count_rew * 100;

    foreach ($newtable as $item){
        $letter_name_for_icon = $item->name;

        $rest = substr(  $item->updated_at, 0, 10);

        $rest_main = explode('-',$rest);
        $half_data = substr($rest_main[0],2);
        $data_ok = $rest_main[1] . '/'. $rest_main[2] . '/'.$half_data;

        $result .= '
          <div data-id ="'. $item->id .'" class="yotpo_reviews__ovner">
                 <div class="yotpo_reviews__icon">
                <span class="yotpo-icon yotpo-icon-circle-checkmark yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip" data-target-container="yotpo-header"></span>

                     '. $letter_name_for_icon{0} .'
                </div>
                <div class="yotpo_reviews__other">
                 <div class="yotpo-tool-tip" data-user-type="yotpo-verified-buyer"  >
                    <div class="tool-tip-header">
                            <span>
                                What is a
                            </span>
                            <span class="header-green">
                                Verified Buyer
                            </span>
                        </div>
                        <div class="tool-tip-content">
                            A Verified Buyer is a user who has purchased the reviewed product through our store.
                        </div>
                    </div>
                    <div class="yotpo_reviews__info">
                        <div class="yotpo_reviews__name">
                            <span>'.   $item->name .'</span>
                            <span class="y-label yotpo-user-title yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip">Verified Buyer</span>
                        </div>
                        <div class="yotpo_reviews__time">
                        '.   $data_ok .'
                        </div>
                    </div>
                    <div class="yotpo_reviews__score">
                        '. stars_rating_func_yotpo($item->score) .'
                    </div>
                    <div class="yotpo_reviews__title">
                        '. $item->title .'
                    </div>
                    <div class="yotpo_reviews__content">
                        '. $item->content .'
                    </div>
                    <div class="yotpo_reviews__product_name">
                        <a href="
                            '. getpermalink_byid($item->sku) .'
                        " target="_blank">
                            '. $item->product_name .'
                        </a>
                    </div>
                    <div class="all_customs">
                        <div class="yotpo_reviews__product_name yotpo_reviews__product_nameshare">
                            <div class="socisl_bbt_spanshare" data_share="'. $item->pid .'">
                                <span class="yotpo-icon yotpo-icon-share"></span>
                                 Share
                            </div>
                            <div class="socisl_bbt div_'. $item->pid .'">
                                <a target="_blank" href="http://www.facebook.com/share.php?u='. getpermalink_byid($item->sku) .'&title='. $item->title .'">Facebook</a>
                                <a target="_blank"  rel="nofollow" class="bt" title="поделиться в Твиттере" href="https://twitter.com/intent/tweet?url='. getpermalink_byid($item->sku) .'&amp;text='. $item->title .'">twitter</a>
                                <a class="social-sharing-icon social-sharing-icon-linkedin" target="_new" href="http://www.linkedin.com/shareArticle?mini=true&url='. getpermalink_byid($item->sku) .'&title='. $item->title .'&source='. $item->content .'">LinkedIn</a>
                                <a class="social-sharing-icon social-sharing-icon-google-plus" target="_new" href="https://plus.google.com/share?url='. getpermalink_byid($item->sku) .'">Google</a>
                            </div>
                        </div>
                        <div class="helpful_div">
                            <div class="helpstitle">
                                Was This Review Helpful?
                            </div>
                            <div class="helpful_div_bt_res">
                                <div class="helpful_div_bt_lilke">
                                    <span data-id_answer ="'. $item->id .'"  class="yotpo-icon yotpo-icon-thumbs-up"></span>
                                    <span class="counts_thumbs_up">'. $item->like_count .'</span>
                                </div>
                                <div class="helpful_div_bt_res_dislike">
                                    <span data-id_answer ="'. $item->id .'" class="yotpo-icon yotpo-icon-thumbs-down"></span>
                                    <span class="counts_thumbs_down">'. $item->dislike_count .'</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>';

    }

    $rt_cc = ceil($count_rew/30);
    if($rt_cc != '1'){
        $count_dots="";
        for($i=1; $i<=ceil($count_rew/30); $i++){
            $count_dots .= '
            <div data-rowspage="'.$i.'" data-hash="yotpo-page-'.$i.'" class="all_block_items">
                <a href="#yotpo-page-'.$i.'">'.$i.'</a>
            </div>';
        }
    }

    $html_slick_reviews = '
   <div class="yotpo-label yotpo_center__div">
            <div class="yotpo-label_logo">
            <span class="yotpo-logo-title">
                <a class="" target="_blank" href="http://my.yotpo.com/landing_page?redirect=https%3A%2F%2Fwww.yotpo.com%2Fverified-product-reviews-by-yotpo%2F&amp;utm_campaign=branding_link_reviews_widget_v2&amp;utm_medium=widget&amp;utm_source=viabestbuy.com">
                    Powered by
                </a>
                <div class="yotpo-icon-btn-big transparent-color-btn"> <a href="http://my.yotpo.com/landing_page?redirect=https%3A%2F%2Fwww.yotpo.com%2Fverified-product-reviews-by-yotpo%2F&amp;utm_campaign=branding_link_reviews_widget_v2&amp;utm_medium=widget&amp;utm_source=viabestbuy.com" class="yotpo-icon yotpo-icon-yotpo-logo" target="_blank"> <span class="sr-only"></span> </a> </div>
            </span>
            </div>
    </div>

    <div class="yotpo-bottomline-box-1 yotpo-stars-and-sum-reviews">
        <div class="yotpo-stars">
           '.stars_rating_func_yotpo(5).'
        </div>
        <div class="yotpo-sum-reviews" style="">
            <span class="font-color-gray based-on">'. $count_rew .' Reviews</span>
        </div>
    </div>
    <div class="yotpo_block_items_level">
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(5).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score5 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent5.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(4).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score4 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent4.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(3).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score3 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent3.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(2).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score2 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent2.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(1).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score1 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent1.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="barc_yoptpo_div">
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Product Quality:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Sexual Performance:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Shipping:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
    </div>
     <div class="yotpo-nav-wrapper">
         <span>Reviews</span>
         <span class="nav-tab-sum yotpo-reviews-nav-tab-sum">('.$count_rew.')</span>
     </div>
        <div class="all_block_items_yo">
            <div  data-page="1" class="all_block_items__yotpo all_block_items__yotpo1">
                '. $result .'
            </div>
        </div>
        <div class="next_arrows_outer">
            <div class="next_arrows owl-carousel ">
                '. $count_dots  .'
            </div>
        </div>';

    return $html_slick_reviews;
}
add_shortcode('vardenafil_category_products', 'Vardenafil_func');


//функция для вызова на страницах категории
function Vardenafil_func_category($category_name){
    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom';
    $result ='';
    $newtable = $wpdb->get_results( "SELECT * FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%$category_name%' ORDER BY `wp_yotpo_custom`.`updated_at` DESC LIMIT 30" );

    $count_rew = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%$category_name%'");

    $count_rew_score5 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%$category_name%' and `score` = 5");
    $count_rew_score4 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%$category_name%' and `score` = 4");
    $count_rew_score3 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%$category_name%' and `score` = 3");
    $count_rew_score2 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%$category_name%' and `score` = 2");
    $count_rew_score1 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%$category_name%' and `score` = 1");

    $count_rew_percent5 = $count_rew_score5 / $count_rew * 100;
    $count_rew_percent4 = $count_rew_score4 / $count_rew * 100;
    $count_rew_percent3 = $count_rew_score3 / $count_rew * 100;
    $count_rew_percent2 = $count_rew_score2 / $count_rew * 100;
    $count_rew_percent1 = $count_rew_score1 / $count_rew * 100;

    foreach ($newtable as $item){
        $letter_name_for_icon = $item->name;

        $rest = substr(  $item->updated_at, 0, 10);

        $rest_main = explode('-',$rest);
        $half_data = substr($rest_main[0],2);
        $data_ok = $rest_main[1] . '/'. $rest_main[2] . '/'.$half_data;

        $result .= '
          <div data-id ="'. $item->id .'" class="yotpo_reviews__ovner">
                   <div class="yotpo_reviews__icon">
                <span class="yotpo-icon yotpo-icon-circle-checkmark yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip" data-target-container="yotpo-header"></span>

                     '. $letter_name_for_icon{0} .'
                </div>
                <div class="yotpo_reviews__other">
                 <div class="yotpo-tool-tip" data-user-type="yotpo-verified-buyer"  >
                <div class="tool-tip-header">
                    <span>
                        What is a
                    </span>
                    <span class="header-green">
                        Verified Buyer
                    </span>
                </div>
                <div class="tool-tip-content">
                    A Verified Buyer is a user who has purchased the reviewed product through our store.
                </div>
            </div>
                    <div class="yotpo_reviews__info">
                        <div class="yotpo_reviews__name">
                            <span>'.   $item->name .'</span>
                            <span class="y-label yotpo-user-title yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip">Verified Buyer</span>
                        </div>
                        <div class="yotpo_reviews__time">
                        '.   $data_ok .'
                        </div>
                    </div>
                    <div class="yotpo_reviews__score">
                        '. stars_rating_func_yotpo($item->score) .'
                    </div>
                    <div class="yotpo_reviews__title">
                        '. $item->title .'
                    </div>
                    <div class="yotpo_reviews__content">
                        '. $item->content .'
                    </div>
                    <div class="yotpo_reviews__product_name">
                        <a href="
                            '. getpermalink_byid($item->sku) .'
                        " target="_blank">
                            '. $item->product_name .'
                        </a>
                    </div>
                    <div class="all_customs">
                        <div class="yotpo_reviews__product_name yotpo_reviews__product_nameshare">
                            <div class="socisl_bbt_spanshare" data_share="'. $item->pid .'">
                                <span class="yotpo-icon yotpo-icon-share"></span>
                                 Share
                            </div>
                            <div class="socisl_bbt div_'. $item->pid .'">
                                <a target="_blank" href="http://www.facebook.com/share.php?u='. getpermalink_byid($item->sku) .'&title='. $item->title .'">Facebook</a>
                                <a target="_blank"  rel="nofollow" class="bt" title="поделиться в Твиттере" href="https://twitter.com/intent/tweet?url='. getpermalink_byid($item->sku) .'&amp;text='. $item->title .'">twitter</a>
                                <a class="social-sharing-icon social-sharing-icon-linkedin" target="_new" href="http://www.linkedin.com/shareArticle?mini=true&url='. getpermalink_byid($item->sku) .'&title='. $item->title .'&source='. $item->content .'">LinkedIn</a>
                                <a class="social-sharing-icon social-sharing-icon-google-plus" target="_new" href="https://plus.google.com/share?url='. getpermalink_byid($item->sku) .'">Google</a>
                            </div>
                        </div>
                        <div class="helpful_div">
                            <div class="helpstitle">
                                Was This Review Helpful?
                            </div>
                            <div class="helpful_div_bt_res">
                                <div class="helpful_div_bt_lilke">
                                    <span  data-id_answer ="'. $item->id .'"   class="yotpo-icon yotpo-icon-thumbs-up"></span>
                                    <span class="counts_thumbs_up">'. $item->like_count .'</span>
                                </div>
                                <div class="helpful_div_bt_res_dislike">
                                    <span  data-id_answer ="'. $item->id .'"   class="yotpo-icon yotpo-icon-thumbs-down"></span>
                                    <span class="counts_thumbs_down">'. $item->dislike_count .'</span>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>';
    }

    $rt_cc = ceil($count_rew/30);
    if($rt_cc != '1'){
        $count_dots="";
        for($i=1; $i<=ceil($count_rew/30); $i++){
            $count_dots .= '
            <div data-rowspage="'.$i.'" data-hash="yotpo-page-'.$i.'" class="all_block_items">
                <a href="#yotpo-page-'.$i.'">'.$i.'</a>
            </div>';
        }
    }

    $html_slick_reviews = '
   <div class="yotpo-label yotpo_center__div">
            <div class="yotpo-label_logo">
            <span class="yotpo-logo-title">
                <a class="" target="_blank" href="http://my.yotpo.com/landing_page?redirect=https%3A%2F%2Fwww.yotpo.com%2Fverified-product-reviews-by-yotpo%2F&amp;utm_campaign=branding_link_reviews_widget_v2&amp;utm_medium=widget&amp;utm_source=viabestbuy.com">
                    Powered by
                </a>
                <div class="yotpo-icon-btn-big transparent-color-btn"> <a href="http://my.yotpo.com/landing_page?redirect=https%3A%2F%2Fwww.yotpo.com%2Fverified-product-reviews-by-yotpo%2F&amp;utm_campaign=branding_link_reviews_widget_v2&amp;utm_medium=widget&amp;utm_source=viabestbuy.com" class="yotpo-icon yotpo-icon-yotpo-logo" target="_blank"> <span class="sr-only"></span> </a> </div>
            </span>
            </div>
    </div>

    <div class="yotpo-bottomline-box-1 yotpo-stars-and-sum-reviews">
        <div class="yotpo-stars">
           '.stars_rating_func_yotpo(5).'
        </div>
        <div class="yotpo-sum-reviews" style="">
            <span class="font-color-gray based-on">'. $count_rew .' Reviews</span>
        </div>
    </div>
    <div class="yotpo_block_items_level">
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(5).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score5 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent5.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(4).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score4 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent4.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(3).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score3 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent3.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(2).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score2 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent2.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(1).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score1 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent1.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="barc_yoptpo_div">
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Product Quality:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Sexual Performance:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Shipping:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
    </div>
     <div class="yotpo-nav-wrapper">
         <span>Reviews</span>
         <span class="nav-tab-sum yotpo-reviews-nav-tab-sum">('.$count_rew.')</span>
     </div>
        <div class="all_block_items_yo">
            <div  data-page="1" class="all_block_items__yotpo all_block_items__yotpo1">
                '. $result .'
            </div>
        </div>
        <div class="next_arrows_outer" data-name-category="'.  $category_name  .'">
            <div class="next_arrows owl-carousel ">
                '. $count_dots  .'
            </div>
        </div>
        ';

    return $html_slick_reviews;
}


//главная страница рейтинг отображается при загрузке главной страницы
//add_action( 'wp_ajax_home_page_star_rating_once', 'home_page_star_rating_once' );
//add_action( 'wp_ajax_nopriv_home_page_star_rating_once', 'home_page_star_rating_once' );

// add_action( 'get_footer', 'home_page_star_rating_once' );
function home_page_star_rating_once($content) {
    if (is_front_page()) {

        $total_reviews = get_option('home_page_star__center_all_rating');

        global $wpdb, $post;
        $newtable = $wpdb->get_results("SELECT * FROM `wp_ratings`");
        $rowcount = $wpdb->get_var("SELECT COUNT(*) FROM `wp_ratings`");

        $total_reviews = get_option('home_page_star_rating');
        $count_reviews = get_option('home_page_star__count_rating');

        $resuls = $total_reviews / $count_reviews;

        $letter_name_for_icon = '';
        foreach ($newtable as $item) {
            $letter_name_for_icon = $letter_name_for_icon + (integer)($item->rating_rating);
        }
        $count_end = $rowcount + $count_reviews;

        $search = '.';
        $replace = '';
        $ress = str_replace($search, $replace, $resuls);

        $autor_nickname = get_the_author_meta( 'nickname',  $post->post_author);
        if( has_post_thumbnail( $post->ID ) ){
            $image = '"'.get_the_post_thumbnail_url($post->ID).'"';
        } else {
            $image = '{"@id": "#logo"}';
        }
        $url = get_permalink($post);

        $average_rating = round($resuls, 2);

        $res_html =
            '<div class="post-ratings pal_fin star-' . $ress . '" data-nonce="c99b5a393b">
            <img id="rating_66277_1" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" data-star="1" title="1 Star" onmouseover="current_rating(66277, 1, \'1 Star\');" onmouseout="ratings_off(5, 0, 0);" onclick="rate_post();" onkeypress="rate_post();"  style="cursor: pointer; border: 0px;">
            <img id="rating_66277_2" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" data-star="2" title="2 Stars" onmouseover="current_rating(66277, 2, \'2 Stars\');" onmouseout="ratings_off(5, 0, 0);" onclick="rate_post();" onkeypress="rate_post();"  style="cursor: pointer; border: 0px;">
            <img id="rating_66277_3" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" data-star="3" title="3 Stars" onmouseover="current_rating(66277, 3, \'3 Stars\');" onmouseout="ratings_off(5, 0, 0);" onclick="rate_post();" onkeypress="rate_post();"  style="cursor: pointer; border: 0px;">
            <img id="rating_66277_4" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" data-star="4" title="4 Stars" onmouseover="current_rating(66277, 4, \'4 Stars\');" onmouseout="ratings_off(5, 0, 0);"  onclick="rate_post();" onkeypress="rate_post();" style="cursor: pointer; border: 0px;">
            <img id="rating_66277_5" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" data-star="5" title="5 Stars" onmouseover="current_rating(66277, 5, \'5 Stars\');" onmouseout="ratings_off(5, 0, 0);"  onclick="rate_post();" onkeypress="rate_post();"   style="cursor: pointer; border: 0px;">
            <div class="cenrinial__option">
                (<strong>' . $count_end . '</strong> votes, average: <strong>' . $average_rating . '</strong> out of 5)
            </div>
            <span class="post-ratings-text" id="ratings_66277_text" style="display:none;"></span>
        </div>
        <script type="application/ld+json" class="yotpo-custom-home-page">
            {
                "@context": "http://schema.org",
                "@type": "AggregateRating",
                "ratingValue": "'.$average_rating.'",
                "ratingCount": "'.$count_end.'",
                "bestRating": "5",
                "worstRating": "1",
                "itemReviewed":  "'.$url.'"
            }
        </script>
    ';

    $custom_content = $res_html;
    $custom_content .= $content;
    echo $custom_content;
    }
}

//главная страница рейтинг
add_action( 'wp_ajax_home_page_star_rating', 'home_page_star_rating' );
add_action( 'wp_ajax_nopriv_home_page_star_rating', 'home_page_star_rating' );
function home_page_star_rating(){
    $home_rating = $_POST['home_rating'];

    global $wpdb;
    $newtable = $wpdb->get_results( "SELECT * FROM `wp_ratings`" );
    $rowcount = $wpdb->get_var("SELECT COUNT(*) FROM `wp_ratings`");

    $total_reviews  = get_option('home_page_star_rating') + $home_rating; //рейтинг главной страницы   //сколько звезд по сумме главная страница
    $count_reviews  = get_option('home_page_star__count_rating') + 1; // кол-во проголосовавших на главной   //кол-во проголосовавших

    $resuls = $total_reviews / $count_reviews; // средний рейтинг главной страницы

    $letter_name_for_icon = '';
    foreach ($newtable as $item) {
        $letter_name_for_icon = $letter_name_for_icon + (integer)($item->rating_rating);
    }
    $count_end = $rowcount + $count_reviews;

    update_option( 'home_page_star_rating', $total_reviews);  //сколько звезд главная страница
    update_option( 'home_page_star__count_rating', $count_reviews); //кол-во проголосовавших
    update_option( 'home_page_star__center_all_rating', $resuls); //средний рейтинг по главной странице

    $res="";

    $res_html='<div class="post-ratings pal_fin star-'.$resuls.'" data-nonce="c99b5a393b" >
    <img id="rating_66277_1" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" data-star="1" title="1 Star" onmouseover="current_rating(66277, 1, \'1 Star\');" onmouseout="ratings_off(5, 0, 0);" onclick="rate_post();" onkeypress="rate_post();" style="cursor: pointer; border: 0px;">
    <img id="rating_66277_2" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" data-star="2" title="2 Stars" onmouseover="current_rating(66277, 2, \'2 Stars\');" onmouseout="ratings_off(5, 0, 0);" onclick="rate_post();" onkeypress="rate_post();" style="cursor: pointer; border: 0px;">
    <img id="rating_66277_3" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" data-star="3" title="3 Stars" onmouseover="current_rating(66277, 3, \'3 Stars\');" onmouseout="ratings_off(5, 0, 0);" onclick="rate_post();" onkeypress="rate_post();" style="cursor: pointer; border: 0px;">
    <img id="rating_66277_4" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" data-star="4" title="4 Stars" onmouseover="current_rating(66277, 4, \'4 Stars\');" onmouseout="ratings_off(5, 0, 0);" onclick="rate_post();" onkeypress="rate_post();" style="cursor: pointer; border: 0px;">
    <img id="rating_66277_5" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" data-star="5" title="5 Stars" onmouseover="current_rating(66277, 5, \'5 Stars\');" onmouseout="ratings_off(5, 0, 0);" onclick="rate_post();" onkeypress="rate_post();" style="cursor: pointer; border: 0px;">

     <div class="cenrinial__option">
        (<strong>'.$count_end.'</strong> votes, average: <strong>'.round($resuls, 2).'</strong> out of 5)
     </div>
     <span class="postaverage" style="display: none;"></span>


    ';
    wp_send_json($res_html) ;
    wp_die();

}
//аякс дозагрузка отзывов
add_action( 'wp_ajax_download_new_review_items', 'download_new_review_items' );
add_action( 'wp_ajax_nopriv_download_new_review_items', 'download_new_review_items' );
function download_new_review_items(){

    $cate = get_queried_object();
    $cateID = $cate->term_id;

    $cateID_name = get_the_category_by_ID($cateID);

    $ids = $_POST['ids'];
    $data_page = $_POST['data_page'];
    $data_name_category = $_POST['data_name_category'];

    $exclude_goods = $_POST['extange'];

    if(!isset($data_name_category)){
        $data_name_category = 'vardenafil';
    }

    $trimmed = trim($data_page);
    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom';
    $result ='';
    // SELECT *  FROM `wp_yotpo_custom` WHERE `cur_term`  LIKE '%vardenafil%' and id not in(1,3)
    $offset = ((int)$trimmed - 1)*30;

    if($exclude_goods != ''){
        $newtable = $wpdb->get_results( "SELECT * FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%$data_name_category%' and sku NOT LIKE '%$exclude_goods%'  ORDER BY `wp_yotpo_custom`.`created_at` DESC LIMIT 30 OFFSET $offset" );
    }
    else{
        $newtable = $wpdb->get_results( "SELECT * FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%$data_name_category%' ORDER BY `wp_yotpo_custom`.`created_at` DESC LIMIT 30 OFFSET $offset" );
    }

    foreach ($newtable as $item){
        $letter_name_for_icon = $item->name;

        $rest = substr(  $item->created_at, 0, 10);
        stars_rating_func($item->score);
        $rest_main = explode('-',$rest);
        $half_data = substr($rest_main[0],2);
        $data_ok = $rest_main[1] . '/'. $rest_main[2] . '/'.$half_data;
        $result .= '
              <div data-id ="'. $item->id .'" class="yotpo_reviews__ovner">
                     <div class="yotpo_reviews__icon">
                <span class="yotpo-icon yotpo-icon-circle-checkmark yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip" data-target-container="yotpo-header"></span>
                         '. $letter_name_for_icon{0} .'
                    </div>
                    <div class="yotpo_reviews__other">
                     <div class="yotpo-tool-tip" data-user-type="yotpo-verified-buyer"  >
                        <div class="tool-tip-header">
                                <span>
                                    What is a
                                </span>
                                <span class="header-green">
                                    Verified Buyer
                                </span>
                            </div>
                            <div class="tool-tip-content">
                                A Verified Buyer is a user who has purchased the reviewed product through our store.
                            </div>
                        </div>
                        <div class="yotpo_reviews__info">
                            <div class="yotpo_reviews__name">
                                <span>'.   $item->name .'</span>
                                <span class="y-label yotpo-user-title yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip">Verified Buyer</span>
                            </div>
                            <div class="yotpo_reviews__time">
                            '.   $data_ok .'
                            </div>
                        </div>
                        <div class="yotpo_reviews__score">
                            '. stars_rating_func_yotpo($item->score) .'
                        </div>
                        <div class="yotpo_reviews__title">
                            '. $item->title .'
                        </div>
                        <div class="yotpo_reviews__content">
                            '. $item->content .'
                        </div>
                        <div class="yotpo_reviews__product_name">
                            <a href="
                                '. getpermalink_byid($item->sku) .'
                            " target="_blank">
                                '. $item->product_name .'
                            </a>
                        </div>
                         <div class="all_customs">
                            <div class="yotpo_reviews__product_name yotpo_reviews__product_nameshare">
                                <div class="socisl_bbt_spanshare" data_share="'. $item->pid .'">
                                    <span class="yotpo-icon yotpo-icon-share"></span>
                                     Share
                                </div>
                                 <div class="socisl_bbt div_'. $item->pid .'">
                                    <a target="_blank" href="http://www.facebook.com/share.php?u='. getpermalink_byid($item->sku) .'&title='. $item->title .'">Facebook</a>
                                    <a target="_blank"  rel="nofollow" class="bt" title="поделиться в Твиттере" href="https://twitter.com/intent/tweet?url='. getpermalink_byid($item->sku) .'&amp;text='. $item->title .'">twitter</a>
                                    <a class="social-sharing-icon social-sharing-icon-linkedin" target="_new" href="http://www.linkedin.com/shareArticle?mini=true&url='. getpermalink_byid($item->sku) .'&title='. $item->title .'&source='. $item->content .'">LinkedIn</a>
                                    <a class="social-sharing-icon social-sharing-icon-google-plus" target="_new" href="https://plus.google.com/share?url='. getpermalink_byid($item->sku) .'">Google</a>
                                </div>
                            </div>
                            <div class="helpful_div">
                                <div class="helpstitle">
                                    Was This Review Helpful?
                                </div>
                                <div class="helpful_div_bt_res">
                                     <div class="helpful_div_bt_lilke">
                                        <span  data-id_answer ="'. $item->id .'"   class="yotpo-icon yotpo-icon-thumbs-up"></span>
                                        <span class="counts_thumbs_up">'. $item->like_count .'</span>
                                    </div>
                                    <div class="helpful_div_bt_res_dislike">
                                        <span  data-id_answer ="'. $item->id .'"   class="yotpo-icon yotpo-icon-thumbs-down"></span>
                                        <span class="counts_thumbs_down">'. $item->dislike_count .'</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
    }
    $html_slick_reviews = '
                <div  data-page="'.$trimmed.'" class="all_block_items__yotpo all_block_items__yotpo'.$trimmed.'">
                    '. $result .'
                </div>
            ';
    $response = $html_slick_reviews;
    wp_send_json($response) ;
    wp_die();
}

//не показывать рейтинг для страниц по id страницам
add_filter('the_content', 'vbb_add_rating_to_content');
function vbb_add_rating_to_content($content) {
    /* запоминаем сюда post_ID где уже выводили рейтинг,
    чтобы не дублировать. Такое было на странице /pay-with-bitcoin/ */
    static $posts_ids_printed_ratings = array();

    if(!function_exists('the_ratings')) return $content;

    if(get_query_var( 'amp' )) return $content;

    // $exclude_rating_pages_ids = array(6,7,8,1928,66277,66959,66976,66984,67773,106558,106564,139324,139334);
    //
    // if(is_page( $exclude_rating_pages_ids )) return $content;

    global $post;

    // if('shopannouncements' == $post->post_type) return $content;
    //
    // if('pharmacy_reviews' == $post->post_type) return $content;

    if('post' != $post->post_type) return $content;

    if(in_array($post->ID, $posts_ids_printed_ratings ))  return $content;

    $posts_ids_printed_ratings[] = $post->ID;

    $content = the_ratings('div', 0, false) . $content;

    return $content;
}

//для дублей страниц возвращает 404
add_action('wp', 'do_rewrite_url');
function do_rewrite_url(){
    global $wp_query;

    $current_page_url = $_SERVER['REQUEST_URI'];

    $url_array = array(
        'generic-viagra-reviews-personal-experiences-real-stories/page/',
        'generic-viagra-soft-tabs-reviews/page/',
        'cialis-soft-reviews-ratings/page/',
        'generic-female-viagra-pink-reviews-testimonials/page/',
        'stendra-user-reviews-testimonials-results/page/',
        'priligy-user-reviews-testimonials-experience/page/',
        'modalert-200-reviews-experience/page/',
        'kamagra-user-reviews-online/page/',
        'generic-avanafil-reviews-testimonials/page/',
        'dapoxetine-review-experience-results/page/',
        'generic-cialis-reviews-personal-experiences-real-stories/page/',
        'sildenafil-testimonials-viagra-pills-reviews/page/',
        'tadalafil-testimonials-cialis-pills-personal-experiences-real-stories/page/',
        'vardenafil-testimonials-levitra-pills-experiences-stories/page/',
        'tadapox-testimonials-super-cialis-pills-personal-experiences-real-stories/page/',
        'super-p-force-testimonials-dual-action-pills-personal-experiences-real-stories/page/',
        'super-zhewitra-testimonials-dual-action-pills-personal-experiences-real-stories/page/',
        'modafinil-modalert-reviews/page/',
    );

    foreach($url_array as $usr_real){
        if(strpos( $current_page_url, $usr_real )){

            $url_arr = explode("/", $current_page_url);
            $last_url_segment = $url_arr[count($url_arr)-2];
            if( is_numeric( $last_url_segment ) ) {
                $wp_query->set_404();
                status_header( 404 );
                get_template_part( 404 );
                exit();
            }
        }

    }

}

add_action( 'woocommerce_after_shop_loop', 'vbb_add_tabs_to_product_cat', 45 );
function vbb_add_tabs_to_product_cat()
{
    if(!is_tax( 'product_cat' ) || get_query_var( 'paged' ) !== 0){
        return;
    }

    remove_action( 'woocommerce_after_shop_loop', 'do_woocommerce_after_shop_loop', 100 );

    global $cg_options;

    $cat = get_queried_object();
    
    $term_meta = get_option( "taxonomy_{$cat->term_id}" );

    $custom_term_meta = $term_meta['custom_term_meta'] ? wpautop( do_shortcode(wp_kses_post( $term_meta['custom_term_meta'] ) )): '';
    ?>

    <div class="all_bl_archive">
        <ul class="custom_tabs">
            <li  data-tabs="tab-description"  class="description_tab active">
              <a>Description</a>
            </li>
            <li data-tabs="tab-yotpo_widget" class="yotpo_widget_tab">
                <a >Verified Reviews</a>
            </li>
            <li data-tabs="tab-yotpo_returns" class="yotpo_returns-tab">
                <a href="#tab-returns"> <?php echo $cg_options['returns_tab_title'];?></a>
            </li>
        </ul>

        <div class="all_tabs_description_page">
            <div class="tab-description">
                 <?php echo $custom_term_meta; ?>
            </div>
            <div class="tab-yotpo_widget">
                  <?php echo yotpo_funk_category($cat);  ?>
            </div>
            <div class="tab-yotpo_returns">
                  <?php echo $cg_options['returns_tab_content'];?>
            </div>
        </div>
    </div>
<?php
}

//регистрация шорткода для вывода на фронтенд Super Zhewitra
function Vardenafil_func_once(){
    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom';
    $result ='';
    // SELECT *  FROM `wp_yotpo_custom` WHERE `cur_term`  LIKE '%vardenafil%' and id not in(1,3)
    $newtable = $wpdb->get_results( "SELECT * FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%Super Zhewitra%'  ORDER BY `wp_yotpo_custom`.`updated_at` DESC LIMIT 30" );

    $count_rew = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%Super Zhewitra%'");

    $count_rew_score5 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%Super Zhewitra%' and `score` = 5");
    $count_rew_score4 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%Super Zhewitra%' and `score` = 4");
    $count_rew_score3 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%Super Zhewitra%' and `score` = 3");
    $count_rew_score2 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%Super Zhewitra%' and `score` = 2");
    $count_rew_score1 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE '%Super Zhewitra%' and `score` = 1");

    $count_rew_percent5 = $count_rew_score5 / $count_rew * 100;
    $count_rew_percent4 = $count_rew_score4 / $count_rew * 100;
    $count_rew_percent3 = $count_rew_score3 / $count_rew * 100;
    $count_rew_percent2 = $count_rew_score2 / $count_rew * 100;
    $count_rew_percent1 = $count_rew_score1 / $count_rew * 100;


    foreach ($newtable as $item){
        $letter_name_for_icon = $item->name;

        $rest = substr(  $item->updated_at, 0, 10);

        $rest_main = explode('-',$rest);
        $half_data = substr($rest_main[0],2);
        $data_ok = $rest_main[1] . '/'. $rest_main[2] . '/'.$half_data;



        $result .= '

          <div data-id ="'. $item->id .'" class="yotpo_reviews__ovner">

            <div class="yotpo_reviews__icon">

                <span class="yotpo-icon yotpo-icon-circle-checkmark yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip" data-target-container="yotpo-header"></span>
                   <span class="ketter_name_for_icon">  '. $letter_name_for_icon{0} .' </span>
            </div>
            <div class="yotpo_reviews__other">


            <div class="yotpo-tool-tip" data-user-type="yotpo-verified-buyer"  >
                <div class="tool-tip-header">
                    <span>
                        What is a
                    </span>
                    <span class="header-green">
                        Verified Buyer
                    </span>
                </div>
                <div class="tool-tip-content">
                    A Verified Buyer is a user who has purchased the reviewed product through our store.
                </div>
            </div>

                <div class="yotpo_reviews__info">
                    <div class="yotpo_reviews__name">
                    <span>'.   $item->name .'</span>
                    <span class="y-label yotpo-user-title yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip">Verified Buyer</span>
                    </div>
                    <div class="yotpo_reviews__time">
                    '.   $data_ok .'
                    </div>
                </div>
                <div class="yotpo_reviews__score">
                    '. stars_rating_func_yotpo($item->score) .'
                </div>
                <div class="yotpo_reviews__title">
                    '. $item->title .'
                </div>
                <div class="yotpo_reviews__content">
                    '. $item->content .'
                </div>
                <div class="yotpo_reviews__product_name">
                    <a href="
                        '. getpermalink_byid($item->sku) .'
                    " target="_blank">
                        '. $item->product_name .'
                    </a>
                </div>
                <div class="all_customs">
                    <div class="yotpo_reviews__product_name yotpo_reviews__product_nameshare">
                        <div class="socisl_bbt_spanshare" data_share="'. $item->pid .'">
                            <span class="yotpo-icon yotpo-icon-share"></span>
                             Share
                        </div>

                        <div class="socisl_bbt div_'. $item->pid .'">
                            <a target="_blank" href="http://www.facebook.com/share.php?u='. getpermalink_byid($item->sku) .'&title='. $item->title .'">Facebook</a>
                            <a target="_blank"  rel="nofollow" class="bt" title="поделиться в Твиттере" href="https://twitter.com/intent/tweet?url='. getpermalink_byid($item->sku) .'&amp;text='. $item->title .'">twitter</a>
                            <a class="social-sharing-icon social-sharing-icon-linkedin" target="_new" href="http://www.linkedin.com/shareArticle?mini=true&url='. getpermalink_byid($item->sku) .'&title='. $item->title .'&source='. $item->content .'">LinkedIn</a>

                            <a class="social-sharing-icon social-sharing-icon-google-plus" target="_new" href="https://plus.google.com/share?url='. getpermalink_byid($item->sku) .'">Google</a>


                        </div>


                    </div>
                    <div class="helpful_div">
                        <div class="helpstitle">
                            Was This Review Helpful?
                        </div>
                        <div class="helpful_div_bt_res">
                             <div class="helpful_div_bt_lilke">
                                    <span  data-id_answer ="'. $item->id .'"   class="yotpo-icon yotpo-icon-thumbs-up"></span>
                                    <span class="counts_thumbs_up">'. $item->like_count .'</span>
                                </div>
                                <div class="helpful_div_bt_res_dislike">
                                    <span  data-id_answer ="'. $item->id .'"   class="yotpo-icon yotpo-icon-thumbs-down"></span>
                                    <span class="counts_thumbs_down">'. $item->dislike_count .'</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>

        ';

    }

    $rt_cc = ceil($count_rew/30);
    if($rt_cc != '1'){
        $count_dots="";
        for($i=1; $i<=ceil($count_rew/30); $i++){
            $count_dots .= '
            <div data-rowspage="'.$i.'" data-hash="yotpo-page-'.$i.'" class="all_block_items">
                <a href="#yotpo-page-'.$i.'">'.$i.'</a>
            </div>';
        }
    }

    $html_slick_reviews = '
   <div class="yotpo-label yotpo_center__div">
            <div class="yotpo-label_logo">
            <span class="yotpo-logo-title">
                <a class="" target="_blank" href="http://my.yotpo.com/landing_page?redirect=https%3A%2F%2Fwww.yotpo.com%2Fverified-product-reviews-by-yotpo%2F&amp;utm_campaign=branding_link_reviews_widget_v2&amp;utm_medium=widget&amp;utm_source=viabestbuy.com">
                    Powered by
                </a>
                <div class="yotpo-icon-btn-big transparent-color-btn"> <a href="http://my.yotpo.com/landing_page?redirect=https%3A%2F%2Fwww.yotpo.com%2Fverified-product-reviews-by-yotpo%2F&amp;utm_campaign=branding_link_reviews_widget_v2&amp;utm_medium=widget&amp;utm_source=viabestbuy.com" class="yotpo-icon yotpo-icon-yotpo-logo" target="_blank"> <span class="sr-only"></span> </a> </div>
            </span>
            </div>
    </div>

    <div class="yotpo-bottomline-box-1 yotpo-stars-and-sum-reviews">
        <div class="yotpo-stars">
           '.stars_rating_func_yotpo(5).'
        </div>
        <div class="yotpo-sum-reviews" style="">
            <span class="font-color-gray based-on">'. $count_rew .' Reviews</span>
        </div>
    </div>
    <div class="yotpo_block_items_level">
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(5).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score5 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent5.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(4).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score4 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent4.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(3).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score3 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent3.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(2).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score2 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent2.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(1).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score1 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent1.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="barc_yoptpo_div">
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Product Quality:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Sexual Performance:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Shipping:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
    </div>

     <div class="yotpo-nav-wrapper">
         <span>Reviews</span>
         <span class="nav-tab-sum yotpo-reviews-nav-tab-sum">('.$count_rew.')</span>
     </div>
        <div class="all_block_items_yo">
            <div  data-page="1" class="all_block_items__yotpo all_block_items__yotpo1">
                '. $result .'
            </div>
        </div>
        <div class="next_arrows_outer">
            <div class="next_arrows owl-carousel ">
                '. $count_dots  .'
            </div>
        </div>
        ';

    return $html_slick_reviews;
}
add_shortcode('vardenafil_category_products_once', 'Vardenafil_func_once');


//если лайк
function helpful_div_bt_lilke($dataanswer){

    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom';

    //+1 если лайк
    $count_rew = intval($wpdb->get_var("SELECT `like_count` FROM $name_db WHERE id='$dataanswer'")) + 1;
    $wpdb->update($name_db, array('like_count'=>$count_rew), array( 'ID' => $dataanswer ));

}
//убрать лайк
function helpful_div_bt_lilke_deduct($dataanswer){

    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom';

    //+1 если лайк
    $count_rew = intval($wpdb->get_var("SELECT `like_count` FROM $name_db WHERE id='$dataanswer'")) - 1;
    $wpdb->update($name_db, array('like_count'=>$count_rew), array( 'ID' => $dataanswer ));

}

//поставить дизлайк
function helpful_div_bt_res_dislike($dataanswer){

    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom';

    //+1 если дизлайк
    $count_rew = intval($wpdb->get_var("SELECT `dislike_count` FROM $name_db WHERE id='$dataanswer'")) + 1;
    $wpdb->update($name_db, array('dislike_count'=>$count_rew), array( 'ID' => $dataanswer ));

}
//убрать дизлайк
function helpful_div_bt_res_dislike_deduct($dataanswer){

    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom';

    //+1 если дизлайк
    $count_rew = intval($wpdb->get_var("SELECT `dislike_count` FROM $name_db WHERE id='$dataanswer'")) - 1;
    $wpdb->update($name_db, array('dislike_count'=>$count_rew), array( 'ID' => $dataanswer ));
}

add_action( 'wp_ajax_helpful_div_bt_res__all', 'helpful_div_bt_res__all' );
add_action( 'wp_ajax_nopriv_helpful_div_bt_res__all', 'helpful_div_bt_res__all' );
function helpful_div_bt_res__all(){
    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom';

    $dataanswer = $_POST['dataanswer'];

    $add_like = $_POST['add_like'];//увеличить лайк
    $reduce_like = $_POST['reduce_like']; //уменьшить лайк
    $add_dislike = $_POST['add_dislike'];//увеличить дизлайк
    $reduce_dislike = $_POST['reduce_dislike'];//уменьшить дизлайк


    if($add_like == 'true'){
        helpful_div_bt_lilke($dataanswer);
    }
    if($reduce_like == 'true'){
        helpful_div_bt_lilke_deduct($dataanswer);
    }
    if($add_dislike == 'true'){
        helpful_div_bt_res_dislike($dataanswer);
    }
    if($reduce_dislike == 'true'){
        helpful_div_bt_res_dislike_deduct($dataanswer);
    }


    $count_rew_like = intval($wpdb->get_var("SELECT `like_count` FROM $name_db WHERE id='$dataanswer'"));
    $count_rew_dislike = intval($wpdb->get_var("SELECT `dislike_count` FROM $name_db WHERE id='$dataanswer'"));

    $resulst ='<div class="helpful_div_bt_lilke">
        <span data-id_answer="'.$dataanswer.'" class="yotpo-icon yotpo-icon-thumbs-up"></span>
        <span class="counts_thumbs_up">'.$count_rew_like.'</span>
    </div>
    <div class="helpful_div_bt_res_dislike">
        <span data-id_answer="'.$dataanswer.'" class="yotpo-icon yotpo-icon-thumbs-down"></span>
        <span class="counts_thumbs_down">'.$count_rew_dislike.'</span>
    </div>';


    wp_send_json($resulst) ;
    die();
}

//шорткод с указанием id категории
function yotpo_custom_intenge( $atts ) {
    $set_attr = shortcode_atts( array(
        'id' => '473',
    ), $atts );

    //!!!если у всловии показать только определенные категории
    if($atts[id] != "" && $atts[besides] == ""){
        $set_attr_category = explode(",", $set_attr['id']);

        $array_category = "";
        foreach($set_attr_category as $category){
            $cat_name =  get_the_category_by_id($category);
            $array_category .= $cat_name . ",";

        }
        $str = substr($array_category, 0, -1);
        $set_attr_category = explode(",", $array_category);
        $trimmed = array_pop($set_attr_category);

        //прописываем условия выбора из базы данных
        $begin_line = "SELECT *  FROM wp_yotpo_custom WHERE cur_term LIKE" . ' ' . "'%" .  $set_attr_category[0] . "%'" ;
        $begin_line_continue = "";
        if(count($set_attr_category) > 1){
            for($i=1; $i<=count($set_attr_category) -1; $i++){
                $begin_line_continue .= " " . "OR `cur_term` LIKE" . ' ' . "'%" .  $set_attr_category[$i] . "%'";
            }
        }

        $end_line = " ORDER BY `wp_yotpo_custom`.`created_at` DESC LIMIT 30";
        //готовая строка запроса
        $result_line = $begin_line . $begin_line_continue . $end_line;
        global $wpdb;
        $name_db = $wpdb->prefix . 'yotpo_custom';
        $result ='';
        $newtable = $wpdb->get_results($result_line);

        //!!!!!!выборка для звезд
        $begin_line_star = "SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE ( `cur_term` LIKE" . ' ' . "'%" .  $set_attr_category[0] . "%'" ;
        //выборка для подсчета кол-ва отзывов
        $begin_line_rew = "SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE" . ' ' . "'%" .  $set_attr_category[0] . "%'" ;
        $begin_line_continue_star = "";
        if(count($set_attr_category) > 1){
            for($i=1; $i<=count($set_attr_category) -1; $i++){
                $begin_line_continue_star .= " " . "OR `cur_term` LIKE" . ' ' . "'%" .  $set_attr_category[$i] . "%'";
            }
        }
        $end_line_star5 = ") and `score` = 5";
        $end_line_star4 = ") and `score` = 4";
        $end_line_star3 = ") and `score` = 3";
        $end_line_star2 = ") and `score` = 2";
        $end_line_star1 = ") and `score` = 1";

        $result_line_star5 = $begin_line_star . $begin_line_continue_star . $end_line_star5;
        $result_line_star4 = $begin_line_star . $begin_line_continue_star . $end_line_star4;
        $result_line_star3 = $begin_line_star . $begin_line_continue_star . $end_line_star3;
        $result_line_star2 = $begin_line_star . $begin_line_continue_star . $end_line_star2;
        $result_line_star1 = $begin_line_star . $begin_line_continue_star . $end_line_star1;
        $category_all_name = "";
        $category_all_name .= $set_attr_category[0];
    }
    //!!!если у всловии показать все кроме
    if($atts['id'] == "" && $atts['besides'] != ""){

        $set_attr_category = explode(",", $atts['besides']);

        $array_category = "";
        foreach($set_attr_category as $category){
            //        echo $category;
            $cat_name =  get_the_category_by_id($category);
            $array_category .= $cat_name . ",";

        }
        $str = substr($array_category, 0, -1);
        $set_attr_category = explode(",", $array_category);
        //  print_r ($set_attr_category);
        $trimmed = array_pop($set_attr_category);

        //прописываем условия выбора из базы данных
        $begin_line = "SELECT *  FROM wp_yotpo_custom WHERE cur_term NOT LIKE" . ' ' . "'%" .  $set_attr_category[0] . "%'" ;
        $begin_line_continue = "";
        if(count($set_attr_category) > 1){
            for($i=1; $i<=count($set_attr_category) -1; $i++){
                $begin_line_continue .= " " . "OR `cur_term` NOT LIKE" . ' ' . "'%" .  $set_attr_category[$i] . "%'";
            }
        }
        $end_line = " ORDER BY `wp_yotpo_custom`.`created_at` DESC LIMIT 30";
        //готовая строка запроса
        $result_line = $begin_line . $begin_line_continue . $end_line;

        global $wpdb;
        $name_db = $wpdb->prefix . 'yotpo_custom';
        $result ='';
        $newtable = $wpdb->get_results($result_line);

        //!!!!!!выборка для звезд
        $begin_line_star = "SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE ( `cur_term` NOT LIKE" . ' ' . "'%" .  $set_attr_category[0] . "%'" ;
        //выборка для подсчета кол-ва отзывов
        $begin_line_rew = "SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` NOT LIKE" . ' ' . "'%" .  $set_attr_category[0] . "%'" ;
        $begin_line_continue_star = "";
        if(count($set_attr_category) > 1){
            for($i=1; $i<=count($set_attr_category) -1; $i++){
                $begin_line_continue_star .= " " . "OR `cur_term` NOT LIKE" . ' ' . "'%" .  $set_attr_category[$i] . "%'";
            }
        }

        $end_line_star5 = ") and `score` = 5";
        $end_line_star4 = ") and `score` = 4";
        $end_line_star3 = ") and `score` = 3";
        $end_line_star2 = ") and `score` = 2";
        $end_line_star1 = ") and `score` = 1";

        $result_line_star5 = $begin_line_star . $begin_line_continue_star . $end_line_star5;
        $result_line_star4 = $begin_line_star . $begin_line_continue_star . $end_line_star4;
        $result_line_star3 = $begin_line_star . $begin_line_continue_star . $end_line_star3;
        $result_line_star2 = $begin_line_star . $begin_line_continue_star . $end_line_star2;
        $result_line_star1 = $begin_line_star . $begin_line_continue_star . $end_line_star1;
    }
    //если параметров нет и нужно вывести все отзывы по всем категориям
    if($atts['id'] == "" && $atts['besides'] == ""){
        //прописываем условия выбора из базы данных
        $begin_line = "SELECT *  FROM wp_yotpo_custom";
        $end_line = " ORDER BY `wp_yotpo_custom`.`updated_at` DESC LIMIT 30";
        //готовая строка запроса
        $result_line = $begin_line . $end_line;

        global $wpdb;
        $name_db = $wpdb->prefix . 'yotpo_custom';
        $result ='';
        $newtable = $wpdb->get_results($result_line);
        //!!!!!!выборка для звезд
        $begin_line_star = "SELECT COUNT(id) FROM `wp_yotpo_custom`";
        //выборка для подсчета кол-ва отзывов
        $begin_line_rew = "SELECT COUNT(id) FROM `wp_yotpo_custom`" ;

        $end_line_star5 = " WHERE `score` = 5";
        $end_line_star4 = " WHERE `score` = 4";
        $end_line_star3 = " WHERE `score` = 3";
        $end_line_star2 = " WHERE `score` = 2";
        $end_line_star1 = " WHERE `score` = 1";

        $result_line_star5 = $begin_line_star . $end_line_star5;
        $result_line_star4 = $begin_line_star . $end_line_star4;
        $result_line_star3 = $begin_line_star . $end_line_star3;
        $result_line_star2 = $begin_line_star . $end_line_star2;
        $result_line_star1 = $begin_line_star . $end_line_star1;
    }

    if($atts['id'] == "" && $atts['besides'] == "" && $atts['exclude_goods'] != ""){
        //прописываем условия выбора из базы данных
        $begin_line = "SELECT *  FROM wp_yotpo_custom";
        $end_line = " ORDER BY `wp_yotpo_custom`.`updated_at` DESC LIMIT 30";
        //готовая строка запроса
        $result_line = $begin_line . $end_line;

        global $wpdb;
        $name_db = $wpdb->prefix . 'yotpo_custom';
        $result ='';
        $newtable = $wpdb->get_results($result_line);
        //!!!!!!выборка для звезд
        $begin_line_star = "SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `sku` NOT LIKE " . $atts['exclude_goods'];
        //выборка для подсчета кол-ва отзывов
        $begin_line_rew = "SELECT COUNT(id) FROM `wp_yotpo_custom`  WHERE `sku` NOT LIKE " . $atts['exclude_goods'];

        $end_line_star5 = " and `score` = 5";
        $end_line_star4 = " and `score` = 4";
        $end_line_star3 = " and `score` = 3";
        $end_line_star2 = " and `score` = 2";
        $end_line_star1 = " and `score` = 1";

        $result_line_star5 = $begin_line_star . $end_line_star5;
        $result_line_star4 = $begin_line_star . $end_line_star4;
        $result_line_star3 = $begin_line_star . $end_line_star3;
        $result_line_star2 = $begin_line_star . $end_line_star2;
        $result_line_star1 = $begin_line_star . $end_line_star1;
    }


    if($atts['id'] != "" && $atts['besides'] == "" && $atts['exclude_goods'] != ""){
        $set_attr_category = explode(",", $set_attr['id']);
        $set_exclude_goods = explode(",", $atts['exclude_goods']);
        //категории
        $array_category = "";
        foreach($set_attr_category as $category){
            $cat_name =  get_the_category_by_id($category);
            $array_category .= $cat_name . ",";
        }
        //категории
        $str = substr($array_category, 0, -1);
        $set_attr_category = explode(",", $array_category);
        $trimmed = array_pop($set_attr_category);
        //товар
        $str = substr($atts['exclude_goods'], 0, -1);
        $set_exclude_goods_ar = explode(",", $atts['exclude_goods']);
        $trimmed_exclude_goods = array_pop($atts['exclude_goods']);

        $category_all_name = "";

        //прописываем условия выбора из базы данных
        $begin_line = "SELECT *  FROM wp_yotpo_custom WHERE cur_term LIKE" . ' ' . "'%" .  $set_attr_category[0] . "%'" ;

        $category_all_name .= $set_attr_category[0];
        $begin_line_continue = "";

        if(count($set_attr_category) > 1){
            for($i=1; $i<=count($set_attr_category) -1; $i++){
                $begin_line_continue .= " " . "OR `cur_term` LIKE" . ' ' . "'%" .  $set_attr_category[$i] . "%'";
                if($category_all_name != ''){
                    $category_all_name .= ",";
                }
                $category_all_name .= $set_attr_category[$i];
            }
        }

        //прописываем условия выбора из базы данных + исключение по товару
        $begin_exclude_goods_line = "";
        if(count($set_exclude_goods) > 1){
            for($i=0; $i<=count($set_exclude_goods); $i++){
                $begin_exclude_goods_line .= " " . "and `sku` NOT LIKE" . ' ' . "'%" .  $set_exclude_goods_ar[$i] . "%'";
            }
        }
        if(count($set_exclude_goods) == 1){
             $begin_exclude_goods_line .= " " . "and `sku` NOT LIKE" . ' ' . "'%" .  $set_exclude_goods['0'] . "%'";
        }



        $end_line = " ORDER BY `wp_yotpo_custom`.`created_at` DESC LIMIT 30";
        //готовая строка запроса
        $result_line = $begin_line . $begin_line_continue . $begin_exclude_goods_line . $end_line;

        global $wpdb;
        $name_db = $wpdb->prefix . 'yotpo_custom';
        $result ='';
        $newtable = $wpdb->get_results($result_line);

        //!!!!!!выборка для звезд
        $begin_line_star = "SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE ( `cur_term` LIKE" . ' ' . "'%" .  $set_attr_category[0] . "%'" ;
        //выборка для подсчета кол-ва отзывов
        $begin_line_rew = "SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `cur_term` LIKE" . ' ' . "'%" .  $set_attr_category[0] . "%'" ;
        $begin_line_continue_star = "";
        if(count($set_attr_category) > 1){
            for($i=1; $i<=count($set_attr_category) -1; $i++){
                $begin_line_continue_star .= " " . "OR `cur_term` LIKE" . ' ' . "'%" .  $set_attr_category[$i] . "%'";
            }
        }
        $end_line_star5 = ") and `score` = 5";
        $end_line_star4 = ") and `score` = 4";
        $end_line_star3 = ") and `score` = 3";
        $end_line_star2 = ") and `score` = 2";
        $end_line_star1 = ") and `score` = 1";

        $result_line_star5 = $begin_line_star . $begin_line_continue_star . $begin_exclude_goods_line . $end_line_star5;
        $result_line_star4 = $begin_line_star . $begin_line_continue_star . $begin_exclude_goods_line . $end_line_star4;
        $result_line_star3 = $begin_line_star . $begin_line_continue_star . $begin_exclude_goods_line . $end_line_star3;
        $result_line_star2 = $begin_line_star . $begin_line_continue_star .  $begin_exclude_goods_line . $end_line_star2;
        $result_line_star1 = $begin_line_star . $begin_line_continue_star . $begin_exclude_goods_line . $end_line_star1;
    }


    $total_score = $begin_line_rew . $begin_line_continue_star . $begin_exclude_goods_line;
    //подсчет всех отзывов по запросу

    $count_rew = $wpdb->get_var($total_score);

    $count_rew_score5 = $wpdb->get_var($result_line_star5);
    $count_rew_score4 = $wpdb->get_var($result_line_star4);
    $count_rew_score3 = $wpdb->get_var($result_line_star3);
    $count_rew_score2 = $wpdb->get_var($result_line_star2);
    $count_rew_score1 = $wpdb->get_var($result_line_star1);

    $count_rew_percent5 = $count_rew_score5 / $count_rew * 100;
    $count_rew_percent4 = $count_rew_score4 / $count_rew * 100;
    $count_rew_percent3 = $count_rew_score3 / $count_rew * 100;
    $count_rew_percent2 = $count_rew_score2 / $count_rew * 100;
    $count_rew_percent1 = $count_rew_score1 / $count_rew * 100;

    $average_review = ($count_rew_score5 * 5 + $count_rew_score4 * 4 + $count_rew_score3 * 3 + $count_rew_score2 * 2 +$count_rew_score1) / $count_rew;
    $average_review = round($average_review, 2);

    foreach ($newtable as $item){
        $letter_name_for_icon = $item->name;

        $rest = substr(  $item->created_at, 0, 10);

        $rest_main = explode('-',$rest);
        $half_data = substr($rest_main[0],2);
        $data_ok = $rest_main[1] . '/'. $rest_main[2] . '/'.$half_data;
        $product_category= '';
        $category_isnot= '';
        if($atts['exclude_goods'] != ""){
            $category_isnot .= $atts['exclude_goods'];
        }

        $result .= '
          <div data-id ="'. $item->id .'" class="yotpo_reviews__ovner">

            <div class="yotpo_reviews__icon">

                <span class="yotpo-icon yotpo-icon-circle-checkmark yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip" data-target-container="yotpo-header"></span>
                   <span class="ketter_name_for_icon">  '. $letter_name_for_icon{0} .' </span>
            </div>
            <div class="yotpo_reviews__other">


            <div class="yotpo-tool-tip" data-user-type="yotpo-verified-buyer"  >
                <div class="tool-tip-header">
                    <span>
                        What is a
                    </span>
                    <span class="header-green">
                        Verified Buyer
                    </span>
                </div>
                <div class="tool-tip-content">
                    A Verified Buyer is a user who has purchased the reviewed product through our store.
                </div>
            </div>

                <div class="yotpo_reviews__info">
                    <div class="yotpo_reviews__name">
                    <span>'.   $item->name .'</span>
                    <span class="y-label yotpo-user-title yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip">Verified Buyer</span>
                    </div>
                    <div class="yotpo_reviews__time">
                    '.   $data_ok .'
                    </div>
                </div>
                <div class="yotpo_reviews__score">
                    '. stars_rating_func_yotpo($item->score) .'
                </div>
                <div class="yotpo_reviews__title">
                    '. $item->title .'
                </div>
                <div class="yotpo_reviews__content">
                    '. $item->content .'
                </div>
                <div class="yotpo_reviews__product_name">
                    <a href="
                        '. getpermalink_byid($item->sku) .'
                    " target="_blank">
                        '. $item->product_name .'
                    </a>
                </div>
                <div class="all_customs">
                    <div class="yotpo_reviews__product_name yotpo_reviews__product_nameshare">
                        <div class="socisl_bbt_spanshare" data_share="'. $item->pid .'">
                            <span class="yotpo-icon yotpo-icon-share"></span>
                             Share
                        </div>

                        <div class="socisl_bbt div_'. $item->pid .'">
                            <a target="_blank" href="http://www.facebook.com/share.php?u='. getpermalink_byid($item->sku) .'&title='. $item->title .'">Facebook</a>
                            <a target="_blank"  rel="nofollow" class="bt" title="поделиться в Твиттере" href="https://twitter.com/intent/tweet?url='. getpermalink_byid($item->sku) .'&amp;text='. $item->title .'">twitter</a>
                            <a class="social-sharing-icon social-sharing-icon-linkedin" target="_new" href="http://www.linkedin.com/shareArticle?mini=true&url='. getpermalink_byid($item->sku) .'&title='. $item->title .'&source='. $item->content .'">LinkedIn</a>

                            <a class="social-sharing-icon social-sharing-icon-google-plus" target="_new" href="https://plus.google.com/share?url='. getpermalink_byid($item->sku) .'">Google</a>


                        </div>


                    </div>
                    <div class="helpful_div">
                        <div class="helpstitle">
                            Was This Review Helpful?
                        </div>
                        <div class="helpful_div_bt_res">
                             <div class="helpful_div_bt_lilke">
                                    <span  data-id_answer ="'. $item->id .'"   class="yotpo-icon yotpo-icon-thumbs-up"></span>
                                    <span class="counts_thumbs_up">'. $item->like_count .'</span>
                                </div>
                                <div class="helpful_div_bt_res_dislike">
                                    <span  data-id_answer ="'. $item->id .'"   class="yotpo-icon yotpo-icon-thumbs-down"></span>
                                    <span class="counts_thumbs_down">'. $item->dislike_count .'</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>

        ';

    }

$rt_cc = ceil($count_rew/30);
if($rt_cc != '1'){
    $count_dots="";
    for($i=1; $i<=ceil($count_rew/30); $i++){
        $count_dots .= '
        <div data-rowspage="'.$i.'" data-hash="yotpo-page-'.$i.'" class="all_block_items">
            <a href="#yotpo-page-'.$i.'">'.$i.'</a>
        </div>';
    }
}
    $html_slick_reviews = '
   <div class="yotpo-label yotpo_center__div">
            <div class="yotpo-label_logo">
            <span class="yotpo-logo-title">
                <a class="" target="_blank" href="http://my.yotpo.com/landing_page?redirect=https%3A%2F%2Fwww.yotpo.com%2Fverified-product-reviews-by-yotpo%2F&amp;utm_campaign=branding_link_reviews_widget_v2&amp;utm_medium=widget&amp;utm_source=viabestbuy.com">
                    Powered by
                </a>
                <div class="yotpo-icon-btn-big transparent-color-btn"> <a href="http://my.yotpo.com/landing_page?redirect=https%3A%2F%2Fwww.yotpo.com%2Fverified-product-reviews-by-yotpo%2F&amp;utm_campaign=branding_link_reviews_widget_v2&amp;utm_medium=widget&amp;utm_source=viabestbuy.com" class="yotpo-icon yotpo-icon-yotpo-logo" target="_blank"> <span class="sr-only"></span> </a> </div>
            </span>
            </div>
    </div>

    <div class="yotpo-bottomline-box-1 yotpo-stars-and-sum-reviews">
        <div class="yotpo-stars">
           '.stars_rating_func_yotpo(5).'
        </div>
        <div class="yotpo-sum-reviews" style="">
            <span class="font-color-gray based-on">'. $count_rew .' Reviews</span>
        </div>
    </div>
    <div class="yotpo_block_items_level">
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(5).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score5 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent5.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(4).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score4 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent4.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(3).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score3 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent3.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(2).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score2 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent2.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(1).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score1 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent1.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="barc_yoptpo_div">
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Product Quality:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Sexual Performance:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Shipping:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
    </div>

     <div class="yotpo-nav-wrapper">
         <span>Reviews</span>
         <span class="nav-tab-sum yotpo-reviews-nav-tab-sum">('.$count_rew.')</span>
     </div>

        <div class="all_block_items_yo" data-exchange='.$category_isnot.'>
            <div  data-page="1" class="all_block_items__yotpo all_block_items__yotpo1">
                '. $result .'
            </div>
        </div>
        <div class="next_arrows_outer" data-name-category="'.$category_all_name.'">
            <div class="next_arrows owl-carousel ">
                '. $count_dots  .'
            </div>
        </div>
        ';

    return $html_slick_reviews;




}
//add_shortcode( 'yotpo_custom_shortcode', 'yotpo_custom_intenge' );
add_shortcode( 'yotpo_custom_shortcode', '__return_false' );

add_action('product_cat_add_form_fields', 'wh_taxonomy_add_new_meta_field', 10, 1);
add_action('product_cat_edit_form_fields', 'wh_taxonomy_edit_meta_field', 10, 1);
//Product Cat Create page
function wh_taxonomy_add_new_meta_field() {
    ?>

    <div class="form-field">
        <label for="wh_meta_desc"><?php _e('Meta Description', 'wh'); ?></label>
        <textarea name="wh_meta_desc" id="wh_meta_desc"></textarea>
        <p class="description"><?php _e('Enter a meta description, <= 160 character', 'wh'); ?></p>
    </div>
    <?php
}
//Product Cat Edit page
function wh_taxonomy_edit_meta_field($term) {
    //getting term ID
    $term_id = $term->term_id;
    // retrieve the existing value(s) for this meta field.
    $wh_meta_desc = get_term_meta($term_id, 'wh_meta_desc', true);
    ?>

    <tr class="form-field">
        <th scope="row" valign="top"><label for="wh_meta_desc"><?php _e('Meta Description', 'wh'); ?></label></th>
        <td>
            <textarea name="wh_meta_desc" id="wh_meta_desc"><?php echo esc_attr($wh_meta_desc) ? esc_attr($wh_meta_desc) : ''; ?></textarea>
            <p class="description"><?php _e('Enter a meta description', 'wh'); ?></p>
        </td>
    </tr>
    <?php
}
add_action('edited_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);
add_action('create_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);
// Save extra taxonomy fields callback function.
function wh_save_taxonomy_custom_meta($term_id) {
    $wh_meta_title = filter_input(INPUT_POST, 'wh_meta_title');
    $wh_meta_desc = filter_input(INPUT_POST, 'wh_meta_desc');
    update_term_meta($term_id, 'wh_meta_title', $wh_meta_title);
    update_term_meta($term_id, 'wh_meta_desc', $wh_meta_desc);
}


//add_action( 'woocommerce_before_shop_loop', 'yotpo_custom_rating_category' );
function yotpo_custom_rating_category($product_cat){

    $result = '<div id="yotpo_form_review" data-score="" data-category_id ="'. $product_cat->term_id .'" data-category_name ="'. $product_cat->name .'">
        <div class="form_review text-title">
            Leave a category review
        </div>
        <div class="form_review">
            <input class="yotpo_name_review" type="text" size="40" placeholder="Name">
        </div>
        <div class="form_review">
        </div>
        <div class="form_review">

            <div class="post-ratings pal_fin_category " >
             <span>Number of stars: </span>
                <img id="rating_category_1" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" style="cursor: pointer; border: 0px;">
                <img id="rating_category_2" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" style="cursor: pointer; border: 0px;">
                <img id="rating_category_3" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" style="cursor: pointer; border: 0px;">
                <img id="rating_category_4" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" style="cursor: pointer; border: 0px;">
                <img id="rating_category_5" src="/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif" style="cursor: pointer; border: 0px;">
                <div class="cenrinial__option">
                </div>
                 <span class="post-ratings-text" id="ratings_66277_text" style="display:none;"></span>
            </div>
        </div>
        <div class="form_review">
            <input class="yotpo_title_review" type="text" size="40" placeholder="Title">
        </div>
        <div class="form_review">
            <textarea class="textareaform_review" placeholder="Review"></textarea>
        </div>
        <div class="form_review_button">
            Publish
        </div>
    </div>';

    return $result;

}

add_action( 'woocommerce_before_shop_loop', 'add_stars_category', 55 );
function add_stars_category()
{
    if(!is_tax( 'product_cat') || get_query_var( 'paged' ) !== 0){
        return;
    }

    $cat = get_queried_object();

    global $wpdb;

    $cat_name = $cat->name;

    $cat_reviews_table = $wpdb->prefix . 'yotpo_custom_category';
    
    $result ='';

    $query = "SELECT `score`, COUNT(id) as 'count' FROM `$cat_reviews_table` WHERE `category_name` LIKE '$cat_name' GROUP BY `score`";

    $scores = $wpdb->get_results($query, ARRAY_A);

    $reviews_count = 0;
    $reviews_sum = 0;

    if($scores){
        foreach ($scores as $score) {
            $reviews_count += $score['count'];
            $reviews_sum += $score['count'] * $score['score'];
        }
        
        $res_rating = $reviews_sum / $reviews_count;
    }
    ?>
    <div class="custom_rating_ct">
        <?php echo stars_rating_func_yotpo(ceil($res_rating)); ?>
        <div>
            <?php echo $reviews_count;?> votes, (average: <strong> <?php echo round($res_rating, 2);?> </strong> out of 5)
        </div>
    </div>
    <?php
}

add_action( 'wp_ajax_form_review_button_funk', 'form_review_button_funk' );
add_action( 'wp_ajax_nopriv_form_review_button_funk', 'form_review_button_funk' );
function form_review_button_funk()
{
    switch ($_POST['score_category']) {
        case 'rating_category_1':
            $score_category = 1;
            break;
        case 'rating_category_2':
            $score_category = 2;
            break;
        case 'rating_category_3':
            $score_category = 3;
            break;
        case 'rating_category_4':
            $score_category = 4;
            break;
        case 'rating_category_5':
            $score_category = 5;
            break;
    }

    $yotpo_name_review = $_POST['yotpo_name_review'];
    $yotpo_title_review = $_POST['yotpo_title_review'];
    $textareaform_review = $_POST['textareaform_review'];
    $rew_category_name = $_POST['rew_category_name'];
    $rew_category_id = $_POST['rew_category_id'];
    $datetime = $_POST['datetime'];

    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom_category';

    $wpdb->insert($name_db, array(
        'id' => NULL,
        'created_at' =>$datetime,  //время
        'title' =>$yotpo_title_review, //название
        'category_name' =>$rew_category_name,// название категории
        'category_id' =>$rew_category_id,// id категории
        'content' =>"$textareaform_review", //отзыв
        'score' =>$score_category, // оценка
        'name' =>$yotpo_name_review, //пользователь
        'like_count' =>'0',
        'dislike_count' =>'0'
    ));
}

//вывод отзывов категории
function yotpo_funk_category($category_obj){
    global $wpdb;

    $category_url = apply_filters( 'vbb_sd_link', get_category_link($category_obj->term_id) );
    
    $cat_reviews_table = $wpdb->prefix . 'yotpo_custom_category';

    $result ='';
    
    $category = $category_obj->name;
    
    $count_reviews_query = "SELECT `score`, COUNT(id) as 'count' FROM `$cat_reviews_table` WHERE `category_name` LIKE '$category_obj->name' GROUP BY `score`";

    $scores = $wpdb->get_results($count_reviews_query, ARRAY_A);

    $count_rew = 0;
    $reviews_sum = 0;

    $count_rew_score1 = 0;
    $count_rew_score2 = 0;
    $count_rew_score3 = 0;
    $count_rew_score4 = 0;
    $count_rew_score5 = 0;

    $count_rew_percent1 = 0;
    $count_rew_percent2 = 0;
    $count_rew_percent3 = 0;
    $count_rew_percent4 = 0;
    $count_rew_percent5 = 0;

    if($scores){
        foreach ($scores as $score) {
            $count_rew += $score['count'];
            $reviews_sum += $score['count'] * $score['score'];

            $var_name = 'count_rew_score' . $score['score'];
            $$var_name = $score['count'];
        }
        
        $average_review = $reviews_sum / $count_rew;
    }
    
    $cat_reviews = $wpdb->get_results( "SELECT * FROM `$cat_reviews_table` WHERE `category_name` LIKE '$category' ORDER BY `created_at` desc LIMIT 30" );

    $count_rew_percent5 = $count_rew_score5 / $count_rew * 100;
    $count_rew_percent4 = $count_rew_score4 / $count_rew * 100;
    $count_rew_percent3 = $count_rew_score3 / $count_rew * 100;
    $count_rew_percent2 = $count_rew_score2 / $count_rew * 100;
    $count_rew_percent1 = $count_rew_score1 / $count_rew * 100;

    $rich_snippet_reviews_array = array();

    foreach ($cat_reviews as $item){
        $letter_name_for_icon = $item->name;

        $rest = substr(  $item->created_at, 0, 10);

        $rest_main = explode('-',$rest);
        $half_data = substr($rest_main[0],2);
        $data_ok = $rest_main[1] . '/'. $rest_main[2] . '/'.$half_data;
        $new_arraygor = wp_unslash($item->content);
        $new_arrname = wp_unslash($item->name);
        $new_arrtitle = wp_unslash($item->title);
        $category_link = get_category_link();

        $rich_snippet_reviews_array[] = '
        {
            "@type": "Review",
            "author": "'. htmlspecialchars($new_arrname).'",
            "datePublished": "'.htmlspecialchars($rest).'",
            "description": "'.htmlspecialchars($new_arraygor).'",
            "name": "'.htmlspecialchars($new_arrtitle).'",
            "reviewRating": {
                "@type": "Rating",
                "ratingValue": "'.$item->score.'"
            }
        }';

        $result .= '
          <div data-id ="'. $item->id .'" class="yotpo_reviews__ovner_category">
                 <div class="yotpo_reviews__icon">
                <span class="yotpo-icon yotpo-icon-circle-checkmark yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip" data-target-container="yotpo-header"></span>

                     '. $letter_name_for_icon{0} .'
                </div>
                <div class="yotpo_reviews__other">
                 <div class="yotpo-tool-tip" data-user-type="yotpo-verified-buyer"  >
                    <div class="tool-tip-header">
                            <span>
                                What is a
                            </span>
                            <span class="header-green">
                                Verified Buyer
                            </span>
                        </div>
                        <div class="tool-tip-content">
                            A Verified Buyer is a user who has purchased the reviewed product through our store.
                        </div>
                    </div>
                    <div class="yotpo_reviews__info">
                        <div class="yotpo_reviews__name">
                            <span>'.   $new_arrname .'</span>
                            <span class="y-label yotpo-user-title yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip">Verified Buyer</span>
                        </div>
                        <div class="yotpo_reviews__time">
                        '.   $data_ok .'
                        </div>
                    </div>
                    <div class="yotpo_reviews__score">
                        '. stars_rating_func_yotpo($item->score) .'
                    </div>
                    <div class="yotpo_reviews__title">
                        '. $new_arrtitle .'
                    </div>
                    <div class="yotpo_reviews__content">
                        '. $new_arraygor .'
                    </div>
                    <div class="yotpo_reviews__product_name">
                        <span>
                            '. $item->category_name .'
                        </span>
                    </div>
                    <div class="all_customs_category">
                        <div class="yotpo_reviews__product_name yotpo_reviews__product_nameshare">
                            <div class="socisl_bbt_spanshare" data_share="'. $item->id .'">
                                <span class="yotpo-icon yotpo-icon-share"></span>
                                 Share
                            </div>

                            <div class="socisl_bbt div_'. $item->id .'">
                                <a target="_blank" href="http://www.facebook.com/share.php?u='. $category_link.'&title='. $item->title .'">Facebook</a>
                                <a target="_blank"  rel="nofollow" class="bt" title="поделиться в Твиттере" href="https://twitter.com/intent/tweet?url='. $category_link .'&amp;text='. $item->title .'">twitter</a>
                                <a class="social-sharing-icon social-sharing-icon-linkedin" target="_new" href="http://www.linkedin.com/shareArticle?mini=true&url='. $category_link .'&title='. $item->title .'&source='. $item->content .'">LinkedIn</a>

                                <a class="social-sharing-icon social-sharing-icon-google-plus" target="_new" href="https://plus.google.com/share?url='. $category_link .'">Google</a>


                            </div>


                        </div>
                        <div class="helpful_div">
                            <div class="helpstitle">
                                Was This Review Helpful?
                            </div>
                            <div class="helpful_div_bt_res">
                                <div class="helpful_div_bt_lilke">
                                    <span data-id_answer ="'. $item->id .'"  class="yotpo-icon yotpo-icon-thumbs-up"></span>
                                    <span class="counts_thumbs_up">'. $item->like_count .'</span>
                                </div>
                                <div class="helpful_div_bt_res_dislike">
                                    <span data-id_answer ="'. $item->id .'" class="yotpo-icon yotpo-icon-thumbs-down"></span>
                                    <span class="counts_thumbs_down">'. $item->dislike_count .'</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        ';

    }
    $final_owl_bottom_block="";
    $count_dots="";
    if($count_rew == 0){

        $count_dots = '
            <div class="yotpo_cat_null">
            <div class="yotpo-first-review-stars"> <span class="stars-wrapper"> <span class="yotpo-icon yotpo-icon-star"></span> <span class="yotpo-icon yotpo-icon-star"></span> <span class="yotpo-icon yotpo-icon-star"></span> <span class="yotpo-icon yotpo-icon-star"></span> <span class="yotpo-icon yotpo-icon-star"></span> </span> </div>
                <div class="yotpobuttonlabel_adcat_null">
                    <span class="yotpo-icon yotpo-icon-write"></span>
                    <span class="yotpo-icos">be the first to write a review</span>
                </div>
            </div>';


        $final_owl_bottom_block = '
        <div class="null_rewiew_category" ">
                <div class="ret_ccblock">
                    '. $count_dots  .'
                </div>
            </div>
        ';
    }
    else if($count_rew<=30){
        $count_dots .= '
        <div data-rowspage="1" data-hash="yotpo-page-1" class="all_block_items_category">
            <a href="#yotpo-page-1">1</a>
        </div>';


        $final_owl_bottom_block = '
        <div class="next_arrows_outer yotpo_category_arrows" data-name-category="'. $category .'">
                <div class="next_arrows owl-carousel ">
                    '. $count_dots  .'
                </div>
            </div>
        ';
    }
    else{
        for($i=1; $i<=$count_rew/30+1; $i++){
            $count_dots .= '
        <div data-rowspage="'.$i.'" data-hash="yotpo-page-'.$i.'" class="all_block_items_category">
            <a href="#yotpo-page-'.$i.'">'.$i.'</a>
        </div>';
        }

    $final_owl_bottom_block = '
        <div class="next_arrows_outer yotpo_category_arrows" data-name-category="'. $category .'">
                <div class="next_arrows owl-carousel ">
                    '. $count_dots  .'
                </div>
            </div>
        ';

    }

    $thumbnail_id = get_woocommerce_term_meta( $category_obj->term_id, 'thumbnail_id', true );
    $image = wp_get_attachment_url( $thumbnail_id );
    if($image){
        $image = '"'.$image.'"';
    } else {
        $image = '{"@id": "#logo"}';
    }

    $html_slick_reviews = '';

    $min_max_prices = vbb_get_products_offers_by_category($category_obj);

    $html_slick_reviews .= '
    <script type="application/ld+json" class="yotpo-custom-product-category">
    {
        "@context": "http://schema.org",
        "@type": "Product",
        "@id": "'.$category_url.'",
        "name": "'.htmlspecialchars($category_obj->name).'",
        "description": "'.htmlspecialchars($category_obj->description).'",
        "image": '.$image.',
        "offers": [
            {
                "@type": "AggregateOffer",
                "lowPrice": "'.$min_max_prices['min'].'",
                "highPrice": "'.$min_max_prices['max'].'",
                "offerCount": "'.$min_max_prices['offerCount'].'",
                "priceCurrency": "'.get_woocommerce_currency().'",
                "availability": "https://schema.org/InStock",
                "url": "'.$category_url.'",
                "seller": {
                    "@type": "Organization",
                    "name": "'.get_bloginfo( 'name' ).'",
                    "url": "'.get_bloginfo( 'url' ).'"
                }
            }
        ]';

    if($count_rew){
        $rich_snippet_reviews = implode(',', $rich_snippet_reviews_array);
        $html_slick_reviews .= '
        ,"aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "'.$average_review.'",
                "ratingCount": "'.$count_rew.'",
                "bestRating": "5",
                "worstRating": "1"
            },
        "review":['.$rich_snippet_reviews.']';
    }

    $html_slick_reviews .= '}</script>';

    $html_slick_reviews .= '
   <div class="yotpo-label yotpo_center__div">
            <div class="yotpo-label_logo">
                <span class="yotpo-logo-title">
                    <a class="" target="_blank" href="http://my.yotpo.com/landing_page?redirect=https%3A%2F%2Fwww.yotpo.com%2Fverified-product-reviews-by-yotpo%2F&amp;utm_campaign=branding_link_reviews_widget_v2&amp;utm_medium=widget&amp;utm_source=viabestbuy.com">
                        Powered by
                    </a>
                    <div class="yotpo-icon-btn-big transparent-color-btn"> <a href="http://my.yotpo.com/landing_page?redirect=https%3A%2F%2Fwww.yotpo.com%2Fverified-product-reviews-by-yotpo%2F&amp;utm_campaign=branding_link_reviews_widget_v2&amp;utm_medium=widget&amp;utm_source=viabestbuy.com" class="yotpo-icon yotpo-icon-yotpo-logo" target="_blank"> <span class="sr-only"></span> </a> </div>
                </span>
            </div>
             <div class="yotpo-label_addcategory">
                <div class="yotpobuttonlabel_addcategory">
                    <span class="yotpo-icon yotpo-icon-write"></span>
                    <span class="yotpo-icon-button-text">write a review</span>
                </div>
            </div>

    </div>

    <div class="yotpo-bottomline-box-1 yotpo-stars-and-sum-reviews">
        <div class="yotpo-stars">
           '.stars_rating_func_yotpo(5).'
        </div>
        <div class="yotpo-sum-reviews" style="">
            <span class="font-color-gray based-on">'. $count_rew .' Reviews</span>
        </div>
    </div>
    <div class="yotpo_block_items_level">
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(5).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score5 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent5.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(4).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score4 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent4.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(3).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score3 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent3.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(2).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score2 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent2.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(1).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score1 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent1.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="barc_yoptpo_div">
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Product Quality:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Sexual Performance:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Shipping:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
    </div>

    <div class="yotpo-navrating_category">
         '. yotpo_custom_rating_category($category_obj) .'
          <div class="yotpo-navrating_category_answer"></div>
     </div>

     <div class="yotpo-nav-wrapper">
         <span>Reviews</span>
         <span class="nav-tab-sum yotpo-reviews-nav-tab-sum">('.$count_rew.')</span>
     </div>
        <div class="all_block_items_yo">
            <div  data-page="1" class="all_block_items__yotpo_category all_block_items__yotpo1">
                '. $result .'
            </div>
        </div>
         <div >
                 '.$final_owl_bottom_block.'
        </div>

        ';

    return $html_slick_reviews;
}



//след. функции относятся с лайку/дизлайку категории отзывов

//если лайк
function helpful_div_bt_lilke_category($dataanswer){

    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom_category';

    //+1 если лайк
    $count_rew = intval($wpdb->get_var("SELECT `like_count` FROM $name_db WHERE id='$dataanswer'")) + 1;
    $wpdb->update($name_db, array('like_count'=>$count_rew), array( 'ID' => $dataanswer ));

}
//убрать лайк
function helpful_div_bt_lilke_deduct_category($dataanswer){

    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom_category';

    //+1 если лайк
    $count_rew = intval($wpdb->get_var("SELECT `like_count` FROM $name_db WHERE id='$dataanswer'")) - 1;
    $wpdb->update($name_db, array('like_count'=>$count_rew), array( 'ID' => $dataanswer ));

}

//поставить дизлайк
function helpful_div_bt_res_dislike_category($dataanswer){

    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom_category';

    //+1 если дизлайк
    $count_rew = intval($wpdb->get_var("SELECT `dislike_count` FROM $name_db WHERE id='$dataanswer'")) + 1;
    $wpdb->update($name_db, array('dislike_count'=>$count_rew), array( 'ID' => $dataanswer ));

}
//убрать дизлайк
function helpful_div_bt_res_dislike_deduct_category($dataanswer){

    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom_category';

    //+1 если дизлайк
    $count_rew = intval($wpdb->get_var("SELECT `dislike_count` FROM $name_db WHERE id='$dataanswer'")) - 1;
    $wpdb->update($name_db, array('dislike_count'=>$count_rew), array( 'ID' => $dataanswer ));

}


add_action( 'wp_ajax_helpful_div_bt_res__category', 'helpful_div_bt_res__category' );
add_action( 'wp_ajax_nopriv_helpful_div_bt_res__category', 'helpful_div_bt_res__category' );
function helpful_div_bt_res__category(){
    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom_category';

    $dataanswer = $_POST['dataanswer'];

    $add_like = $_POST['add_like'];//увеличить лайк
    $reduce_like = $_POST['reduce_like']; //уменьшить лайк
    $add_dislike = $_POST['add_dislike'];//увеличить дизлайк
    $reduce_dislike = $_POST['reduce_dislike'];//уменьшить дизлайк


    if($add_like == 'true'){
        helpful_div_bt_lilke_category($dataanswer);
    }
    if($reduce_like == 'true'){
        helpful_div_bt_lilke_deduct_category($dataanswer);
    }
    if($add_dislike == 'true'){
        helpful_div_bt_res_dislike_category($dataanswer);
    }
    if($reduce_dislike == 'true'){
        helpful_div_bt_res_dislike_deduct_category($dataanswer);
    }


    $count_rew_like = intval($wpdb->get_var("SELECT `like_count` FROM $name_db WHERE id='$dataanswer'"));
    $count_rew_dislike = intval($wpdb->get_var("SELECT `dislike_count` FROM $name_db WHERE id='$dataanswer'"));

    $resulst ='<div class="helpful_div_bt_lilke">
        <span data-id_answer="'.$dataanswer.'" class="yotpo-icon yotpo-icon-thumbs-up"></span>
        <span class="counts_thumbs_up">'.$count_rew_like.'</span>
    </div>
    <div class="helpful_div_bt_res_dislike">
        <span data-id_answer="'.$dataanswer.'" class="yotpo-icon yotpo-icon-thumbs-down"></span>
        <span class="counts_thumbs_down">'.$count_rew_dislike.'</span>
    </div>';


    wp_send_json($resulst) ;
    die();
}

add_action( 'wp_ajax_download_new_review_items_category', 'download_new_review_items_category' );
add_action( 'wp_ajax_nopriv_download_new_review_items_category', 'download_new_review_items_category' );
function download_new_review_items_category(){



    $ids = $_POST['ids'];
    $data_page = $_POST['data_page'];
    $data_name_category = $_POST['data_name_category'];

    if(!isset($data_name_category)){
        $data_name_category = 'vardenafil';
    }

    $trimmed = trim($data_page);
    global $wpdb;
    $name_db = $wpdb->prefix . 'yotpo_custom';
    $result ='';
    // SELECT *  FROM `wp_yotpo_custom` WHERE `cur_term`  LIKE '%vardenafil%' and id not in(1,3)
    $offset = ((int)$trimmed - 1)*30;
    $category_link = get_category_link();
    $newtable = $wpdb->get_results( "SELECT * FROM `wp_yotpo_custom_category` WHERE `category_name` LIKE '%$data_name_category%' ORDER BY  `created_at` desc LIMIT 30 OFFSET {$offset} " );
    foreach ($newtable as $item){
        $letter_name_for_icon = $item->name;

        $rest = substr(  $item->created_at, 0, 10);
        stars_rating_func($item->score);
        $rest_main = explode('-',$rest);
        $half_data = substr($rest_main[0],2);
        $data_ok = $rest_main[1] . '/'. $rest_main[2] . '/'.$half_data;

        $new_arraygor = wp_unslash($item->content);
        $new_arrname = wp_unslash($item->name);
        $new_arrtitle = wp_unslash($item->title);


        $result .= '

              <div data-id ="'. $item->id .'" class="yotpo_reviews__ovner_category">
                     <div class="yotpo_reviews__icon">
                <span class="yotpo-icon yotpo-icon-circle-checkmark yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip" data-target-container="yotpo-header"></span>

                         '. $letter_name_for_icon{0} .'
                    </div>
                    <div class="yotpo_reviews__other">
                     <div class="yotpo-tool-tip" data-user-type="yotpo-verified-buyer"  >
                        <div class="tool-tip-header">
                                <span>
                                    What is a
                                </span>
                                <span class="header-green">
                                    Verified Buyer
                                </span>
                            </div>
                            <div class="tool-tip-content">
                                A Verified Buyer is a user who has purchased the reviewed product through our store.
                            </div>
                        </div>
                        <div class="yotpo_reviews__info">
                            <div class="yotpo_reviews__name">
                                <span>'.   $new_arrname .'</span>
                                <span class="y-label yotpo-user-title yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip">Verified Buyer</span>
                            </div>
                            <div class="yotpo_reviews__time">
                            '.   $data_ok .'
                            </div>
                        </div>
                        <div class="yotpo_reviews__score">
                            '. stars_rating_func_yotpo($item->score) .'
                        </div>
                        <div class="yotpo_reviews__title">
                            '. $new_arrtitle .'
                        </div>
                        <div class="yotpo_reviews__content">
                            '. $new_arraygor .'
                        </div>
                        <div class="yotpo_reviews__product_name">
                            <span>
                                '. $item->category_name .'
                            </span>
                        </div>
                         <div class="all_customs">
                            <div class="yotpo_reviews__product_name yotpo_reviews__product_nameshare">
                                <div class="socisl_bbt_spanshare" data_share="'. $item->id .'">
                                    <span class="yotpo-icon yotpo-icon-share"></span>
                                     Share
                                </div>

                                 <div class="socisl_bbt div_'. $item->id .'">
                                    <a target="_blank" href="http://www.facebook.com/share.php?u='. $category_link.'&title='. $item->title .'">Facebook</a>
                                    <a target="_blank"  rel="nofollow" class="bt" title="поделиться в Твиттере" href="https://twitter.com/intent/tweet?url='. $category_link.'&amp;text='. $item->title .'">twitter</a>
                                    <a class="social-sharing-icon social-sharing-icon-linkedin" target="_new" href="http://www.linkedin.com/shareArticle?mini=true&url='. $category_link .'&title='. $item->title .'&source='. $item->content .'">LinkedIn</a>

                                    <a class="social-sharing-icon social-sharing-icon-google-plus" target="_new" href="https://plus.google.com/share?url='. $category_link .'">Google</a>

                                </div>

                            </div>
                            <div class="helpful_div">
                                <div class="helpstitle">
                                    Was This Review Helpful?
                                </div>
                                <div class="helpful_div_bt_res">
                                     <div class="helpful_div_bt_lilke">
                                        <span  data-id_answer ="'. $item->id .'"   class="yotpo-icon yotpo-icon-thumbs-up"></span>
                                        <span class="counts_thumbs_up">'. $item->like_count .'</span>
                                    </div>
                                    <div class="helpful_div_bt_res_dislike">
                                        <span  data-id_answer ="'. $item->id .'"   class="yotpo-icon yotpo-icon-thumbs-down"></span>
                                        <span class="counts_thumbs_down">'. $item->dislike_count .'</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            ';

    }


    $html_slick_reviews = '
                <div  data-page="'.$trimmed.'" class="all_block_items__yotpo_category all_block_items__yotpo'.$trimmed.'">
                    '. $result .'
                </div>
            ';
    $response = $html_slick_reviews;
    wp_send_json($response) ;
    wp_die();
}



//Якорная ссылка!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// add_action( 'wp_ajax_bottomlineclickable', 'bottomlineclickable' );
// add_action( 'wp_ajax_nopriv_bottomlineclickable', 'bottomlineclickable' );
function bottomlineclickable() {
    $postId = $_POST['postId'];
    setup_postdata($postId);

    global $product;


    $category_array = get_the_terms( $postId, 'product_cat' );

    $url_array = array(
        'Vardenafil',
        'Levitra 10 mg',
        'Levitra 20 mg (Bestseller)',
        'Levitra 40 mg',
        'Levitra 60 mg',
        'Levitra Soft',
        'Super Zhewitra',
        'Alternatives to Levitra'
    );

    $response = "dont";
    foreach($category_array as $cat_arr) {
        if(in_array($cat_arr->name,$url_array)){
            $response = "ok";
        }
    }
    echo $response;

    wp_die();
}


//продублировать блоки отзывов
add_action( 'woocommerce_before_single_product_summary', 'duplicate_rewrite__yopto', 10 );
function duplicate_rewrite__yopto()
{

    global $product;
    $url = get_permalink( $product->get_id() );

    $cateID_name = $product->get_title();
    $categ = $product->get_categories();

    $url_array = array(
        'Vardenafil',
        'Levitra 10 mg',
        'Levitra 20 mg (Bestseller)',
        'Levitra 40 mg',
        'Levitra 60 mg',
        'Levitra Soft',
        'Super Zhewitra',
        'Alternatives to Levitra'
    );
    echo '_______________________________________<br/>';

    // foreach($url_array as $usr_real) {
        // if (strpos($categ, $usr_real)){

            global $wpdb;
            $name_db = $wpdb->prefix . 'yotpo_custom';
            $result ='';
            // SELECT *  FROM `wp_yotpo_custom` WHERE `cur_term`  LIKE '%vardenafil%' and id not in(1,3)
            $product_yotpo_reviews = $wpdb->get_results( "SELECT * FROM $name_db WHERE `product_name` LIKE '$cateID_name' ORDER BY `wp_yotpo_custom`.`created_at` DESC  LIMIT 30" );

            $count_rew = $wpdb->get_var("SELECT COUNT(id) FROM $name_db WHERE `product_name` LIKE '$cateID_name'");

            // if(!$count_rew) continue;
            if(!$count_rew) return;

            $count_rew_score5 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `product_name` LIKE '$cateID_name' and `score` = 5");
            $count_rew_score4 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `product_name` LIKE '$cateID_name' and `score` = 4");
            $count_rew_score3 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `product_name` LIKE '$cateID_name' and `score` = 3");
            $count_rew_score2 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `product_name` LIKE '$cateID_name' and `score` = 2");
            $count_rew_score1 = $wpdb->get_var("SELECT COUNT(id) FROM `wp_yotpo_custom` WHERE `product_name` LIKE '$cateID_name' and `score` = 1");

            $count_rew_percent5 = $count_rew_score5 / $count_rew * 100;
            $count_rew_percent4 = $count_rew_score4 / $count_rew * 100;
            $count_rew_percent3 = $count_rew_score3 / $count_rew * 100;
            $count_rew_percent2 = $count_rew_score2 / $count_rew * 100;
            $count_rew_percent1 = $count_rew_score1 / $count_rew * 100;

            $average_review = ($count_rew_score5 * 5 + $count_rew_score4 * 4 + $count_rew_score3 * 3 + $count_rew_score2 * 2 + $count_rew_score1) / $count_rew;
            $average_review = round($average_review, 2);

            foreach ($product_yotpo_reviews as $item){
                $letter_name_for_icon = $item->name;

                $rest = substr(  $item->updated_at, 0, 10);

                $rest_main = explode('-',$rest);
                $half_data = substr($rest_main[0],2);
                $data_ok = $rest_main[1] . '/'. $rest_main[2] . '/'.$half_data;

                $item_name = htmlspecialchars(wp_unslash($item->name));
                $item_content = htmlspecialchars(wp_unslash($item->content));
                $item_title = htmlspecialchars(wp_unslash($item->title));

                $result .= '

          <div class="yotpo_display">
                <div class="yotpo_reviews__icon">
                <span class="yotpo-icon yotpo-icon-circle-checkmark yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip" data-target-container="yotpo-header"></span>
                     '. $letter_name_for_icon{0} .'
                </div>
                <div class="yotpo_reviews__other">
                 <div class="yotpo-tool-tip" data-user-type="yotpo-verified-buyer"  >
                    <div class="tool-tip-header">
                            <span>
                                What is a
                            </span>
                            <span class="header-green">
                                Verified Buyer
                            </span>
                        </div>
                        <div class="tool-tip-content">
                            A Verified Buyer is a user who has purchased the reviewed product through our store.
                        </div>
                    </div>
                    <div class="yotpo_reviews__info">
                        <div class="yotpo_reviews__name">
                            <span>'.   $item->name .'</span>
                            <span class="y-label yotpo-user-title yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip">Verified Buyer</span>
                        </div>
                        <div class="yotpo_reviews__time">
                        '.   $data_ok .'
                        </div>
                    </div>
                    <div class="yotpo_reviews__score">
                        '. stars_rating_func_yotpo($item->score) .'
                    </div>
                    <div class="yotpo_reviews__title">
                        '. $item->title .'
                    </div>
                    <div class="yotpo_reviews__content">
                        '. $item->content .'
                    </div>
                    <div class="yotpo_reviews__product_name">
                        <a href="
                            '. getpermalink_byid($item->sku) .'
                        " target="_blank">
                            '. $item->product_name .'
                        </a>
                    </div>
                    <div class="all_customs">
                        <div class="yotpo_reviews__product_name yotpo_reviews__product_nameshare">
                            <div class="socisl_bbt_spanshare" data_share="'. $item->pid .'">
                                <span class="yotpo-icon yotpo-icon-share"></span>
                                 Share
                            </div>

                            <div class="socisl_bbt div_'. $item->pid .'">
                                <a target="_blank" href="http://www.facebook.com/share.php?u='. getpermalink_byid($item->sku) .'&title='. $item->title .'">Facebook</a>
                                <a target="_blank"  rel="nofollow" class="bt" title="поделиться в Твиттере" href="https://twitter.com/intent/tweet?url='. getpermalink_byid($item->sku) .'&amp;text='. $item->title .'">twitter</a>
                                <a class="social-sharing-icon social-sharing-icon-linkedin" target="_new" href="http://www.linkedin.com/shareArticle?mini=true&url='. getpermalink_byid($item->sku) .'&title='. $item->title .'&source='. $item->content .'">LinkedIn</a>

                                <a class="social-sharing-icon social-sharing-icon-google-plus" target="_new" href="https://plus.google.com/share?url='. getpermalink_byid($item->sku) .'">Google</a>


                            </div>


                        </div>
                        <div class="helpful_div">
                            <div class="helpstitle">
                                Was This Review Helpful?
                            </div>
                            <div class="helpful_div_bt_res">
                                <div class="helpful_div_bt_lilke">
                                    <span data-id_answer ="'. $item->id .'"  class="yotpo-icon yotpo-icon-thumbs-up"></span>
                                    <span class="counts_thumbs_up">'. $item->like_count .'</span>
                                </div>
                                <div class="helpful_div_bt_res_dislike">
                                    <span data-id_answer ="'. $item->id .'" class="yotpo-icon yotpo-icon-thumbs-down"></span>
                                    <span class="counts_thumbs_down">'. $item->dislike_count .'</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        ';

        }

        $rt_cc = ceil($count_rew/30);
        if($rt_cc != '1'){
            $count_dots="";
            for($i=1; $i<=ceil($count_rew/30); $i++){
                $count_dots .= '
                <div data-rowspage="'.$i.'" data-hash="yotpo-page-'.$i.'" class="all_block_items">
                    <a href="#yotpo-page-'.$i.'">'.$i.'</a>
                </div>';
            }
        }

            $html_slick_reviews =
'<div style="display: none !important">
       <div class="yotpo-label yotpo_center__div">
            <div class="yotpo-label_logo">
            <span class="yotpo-logo-title">
                <a class="" target="_blank" href="http://my.yotpo.com/landing_page?redirect=https%3A%2F%2Fwww.yotpo.com%2Fverified-product-reviews-by-yotpo%2F&amp;utm_campaign=branding_link_reviews_widget_v2&amp;utm_medium=widget&amp;utm_source=viabestbuy.com">
                    Powered by
                </a>
                <div class="yotpo-icon-btn-big transparent-color-btn"> <a href="http://my.yotpo.com/landing_page?redirect=https%3A%2F%2Fwww.yotpo.com%2Fverified-product-reviews-by-yotpo%2F&amp;utm_campaign=branding_link_reviews_widget_v2&amp;utm_medium=widget&amp;utm_source=viabestbuy.com" class="yotpo-icon yotpo-icon-yotpo-logo" target="_blank"> <span class="sr-only"></span> </a> </div>
            </span>
            </div>
    </div>

    <div class="yotpo-bottomline-box-1 yotpo-stars-and-sum-reviews">
        <div class="yotpo-stars">
           '.stars_rating_func_yotpo(5).'
        </div>
        <div class="yotpo-sum-reviews" style="">
            <span class="font-color-gray based-on">'. $count_rew .' Reviews</span>
        </div>
    </div>
    <div class="yotpo_block_items_level">
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(5).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score5 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent5.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(4).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score4 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent4.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(3).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score3 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent3.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(2).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score2 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent2.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_star_and_content">
            <div class="yotpo_block_items_level_star">
                  '.stars_rating_func_yotpo(1).'
            </div>
            <div class="yotpo_block_items_level_content">
                 <span>('. $count_rew_score1 .')</span>
            </div>
            <div class="yotpo_block_items_level_percent">
                <div class="yotpo-distributions-graphs">
                    <div class="yotpo-star-distribution-graph yotpo-distribution-clickable yotpo-distribution-unactive" data-score-distribution="5">
                        <div class="yotpo-star-distribution-graph-background"></div>
                            <div class="yotpo-star-distribution-score-wrapper">
                            <div class="yotpo-star-distribution-graph-score" style="width: '.$count_rew_percent1.'%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="barc_yoptpo_div">
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Product Quality:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Sexual Performance:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
        <div class="bars_all_yotpo">
            <div class="all_barc_and_title">
                Shipping:
            </div>
            <div class="all_barc_and_content">
                '.bars_rating_func_yotpo(5).'
            </div>
        </div>
    </div>

     <div class="yotpo-nav-wrapper">
         <span>Reviews</span>
         <span class="nav-tab-sum yotpo-reviews-nav-tab-sum">('.$count_rew.')</span>
     </div>


        <div class="all_block_items_yo">
            <div  data-page="1" class="all_block_items__yotpo all_block_items__yotpo1">
                '. $result .'
            </div>
        </div>
        <div class="next_arrows_outer">
            <div class="next_arrows owl-carousel ">
                '. $count_dots  .'
            </div>
        </div>
          </div>
        ';

            echo $html_slick_reviews;
            return;
        // }

    // }

}

add_filter( 'woocommerce_structured_data_product','vbb_fix_woocommerce_structured_data_product', 10, 2 );
function vbb_fix_woocommerce_structured_data_product($markup, $product)
{
    $product_id = $product->get_id();

    $last_reviews = vbb_get_yotpo_reviews(array('product_id' => $product_id));

    if($last_reviews){

        $scores = vbb_get_yotpo_reviews_score_count(array('product_id' => $product_id));

        $rating_data = vbb_aggregate_rating_data($scores);

        $aggregate_rating = vbb_rich_snippet_aggregate_rating($rating_data);

        if(!$aggregate_rating){
            return $markup;
        }

        $markup['aggregateRating'] = $aggregate_rating;

        $reviews_markup = array();

        foreach ($last_reviews as $review) {
            $reviews_markup[] = array(
                "@type"=> "Review",
                "author"=> htmlspecialchars(wp_unslash($review->name)),
                "datePublished"=> $review->created_at,
                "description"=> htmlspecialchars(wp_unslash($review->content)),
                "name"=> htmlspecialchars(wp_unslash($review->title)),
                "reviewRating"=> array (
                    "@type"=> "Rating",
                    "ratingValue"=> $review->score
                ),
            );
        }
    }

    $markup['review'] = $reviews_markup;

    return $markup;
}

function vbb_get_yotpo_reviews($data){
    $default = array(
        'product_id' => 0,
        'category_name' => '',
        'limit' => 30,
    );

    $atts = shortcode_atts( $default, $data );

    global $wpdb;

    if($atts['product_id']){
        $yotpo_custom_db = $wpdb->prefix . 'yotpo_custom';
        $where = ' WHERE sku = ' . $atts['product_id'] . ' ';
    } elseif ($atts['category_name']) {
        $yotpo_custom_db = $wpdb->prefix . 'yotpo_custom_category';
        $where = ' WHERE category_name = "' . $atts['category_name'] . '" ';
    } else {
        return;
    }

    $query = "SELECT * FROM $yotpo_custom_db $where ORDER BY created_at DESC LIMIT " . $atts['limit'];

    $last_reviews = $wpdb->get_results($query);

    return $last_reviews;
}

function vbb_get_yotpo_reviews_score_count($data){
    global $wpdb;

    $default = array(
        'product_id' => 0,
        'category_name' => ''
    );

    $atts = shortcode_atts( $default, $data );

    global $wpdb;

    if($atts['product_id']){
        $yotpo_custom_db = $wpdb->prefix . 'yotpo_custom';
        $where = ' WHERE sku = ' . $atts['product_id'] . ' ';
    } elseif ($atts['category_name']) {
        $yotpo_custom_db = $wpdb->prefix . 'yotpo_custom_category';
        $where = ' WHERE category_name = "' . $atts['category_name'] . '" ';
    } else {
        return;
    }

    $query = "SELECT score, COUNT(1) as count FROM $yotpo_custom_db $where GROUP BY score";

    $scores = $wpdb->get_results($query);

    return $scores;
}

function vbb_rich_snippet_aggregate_rating($data){
    $default = array(
        "best_rating" => 5,
        "worst_rating" => 1,
        "rating_total" => 0,
        "rating_count" => 0
    );

    if(!$data["rating_total"] || !$data["rating_count"]){
        return;
    }

    $atts = shortcode_atts( $default, $data );

    $rating_average = round($atts['rating_total'] / $atts['rating_count'], 2);

    $aggregate_rating = array(
        "@type" => "AggregateRating",
        "ratingValue" => $rating_average,
        "ratingCount" => $atts['rating_count'],
        "bestRating" => $atts['best_rating'],
        "worstRating" => $atts['worst_rating']
    );

    return $aggregate_rating;
}

function vbb_aggregate_rating_data($scores){
    $rating_count = 0;
    $rating_total = 0;

    foreach ($scores as $score) {
        $rating_count += $score->count;
        $rating_total += $score->count * $score->score;
    }

    $rating_data = array('rating_count' => $rating_count, 'rating_total' => $rating_total);

    return $rating_data;
}

function vbb_get_products_offers_by_category($category_obj){
    $args = array(
        'post_type'             => 'product',
        'posts_per_page'        => -1,
        'product_cat'           => $category_obj->slug

    );

    $posts_products = new WP_Query($args);

    $min_max_prices = array('min' => 0, 'max' => 0);

    $offer_count = 0;

    foreach ($posts_products->posts as $post) {
        $product = wc_get_product( $post );

        if ($product->is_type( 'variable' ))
        {
            $available_variations = $product->get_available_variations();

            foreach ($available_variations as $variation){
                $variation_price = $variation['display_price'] ? $variation['display_price'] : $variation['display_regular_price'];

                if(!$variation_price) continue;

                $offer_count++;

                if( !$min_max_prices['min'] ) {
                    $min_max_prices['min'] = $variation_price;
                }

                $min_max_prices['min'] = min($variation_price, $min_max_prices['min']);

                $min_max_prices['max'] = max($variation_price, $min_max_prices['max']);
            }

            continue;
        }

        $price = $product->get_price() ? $product->get_price() : $product->get_regular_price();

        if( !$price ) continue;

        $offer_count++;

        if( !$min_max_prices['min'] )
        {
            $min_max_prices['min'] = $price;
        }

        $min_max_prices['min'] = min($price, $min_max_prices['min']);

        $min_max_prices['max'] = max($price, $min_max_prices['max']);

    }

    $min_max_prices['offerCount'] = $offer_count;

    return $min_max_prices;
}

add_filter('post_class', 'vbb_remove_hentry', 20);
function vbb_remove_hentry($classes) {
    if (($key = array_search('hentry', $classes)) !== false) {
        unset( $classes[$key] );
    }
    return $classes;
}

if ( !function_exists( 'cg_posted_on' ) ) :

    /**
     * Prints HTML with meta information for the current post-date/time and author.
     */
    function cg_posted_on() {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

        $time_string = sprintf( $time_string, esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ), esc_attr( get_the_modified_date( 'c' ) ), esc_html( get_the_modified_date() )
        );

        printf( __( '<span class="posted-on">Posted on %1$s</span><span class="byline"> by %2$s</span>', 'commercegurus' ), sprintf( '<a href="%1$s" rel="bookmark">%2$s</a>', esc_url( get_permalink() ), $time_string
                ), sprintf( '<span class="author"><a class="url fn n" href="%1$s">%2$s</a></span>', esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ), esc_html( get_the_author() )
                )
        );
    }

endif;

add_filter( 'vbb_sd_link', function($link){
    if( is_tax( 'product_cat' ) && class_exists( 'wcRewrite' ) ) {
        $link = wcRewrite::url()['main'];
    }
    return $link;
} );