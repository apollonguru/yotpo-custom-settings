<?php

function page_settings_linxact(){
    wp_enqueue_script( 'yotpo_custom' );
    wp_enqueue_script( 'teatherjs' );
    wp_enqueue_script( 'bootstrapjs' );
    wp_enqueue_script( 'datatablesjs' );
    wp_enqueue_script( 'userslistjs' );
    wp_enqueue_script( 'linxact' );
    wp_enqueue_style( 'linxact' );


    ?>


    <div class="wrap">

        <div class="row">

            <div class="col-sm-6">
                <h1>Settings</h1>
            </div>

            <div class="col-sm-6 text-right">
                <div class="email_admin email">
                   API Key
                </div>
                <div id="block_yp_api_key" class="button_blocks">
                    <div class="email_user_sl">
                        <input id="app_key" type="text" size="60" placeholder="Введите API Key"
                        value="<?php echo get_option('yotpo_plugin_appkey'); ?>">
                    </div>
                    <button id="app_key_send" class="block_user_add_bt_admin">
                        Save
                    </button>
                </div>

            </div>
            <div class="col-sm-6 text-right">
                <div class="email_admin email">
                    Secret API Key
                </div>
                <div id="block_yp_secret_key" class="button_blocks">
                    <div class="email_user_sl">
                        <input id="secretkey" type="text" size="60" placeholder="Secret API Key"
                               value="<?php echo get_option('yotpo_plugin_secretkey'); ?>">
                    </div>
                    <button id="app_secretkey_send" class="block_user_add_bt_admin">
                        Save
                    </button>
                </div>

            </div>

            <div class="col-sm-6 text-top">
                <div class="email_admin email">
                    Launch Schedule
                </div>
                <div id="block_yp_cron_time" class="button_blocks">

                    <?php $cron_hours = (int) get_option('yotpo_plugin_time_update_count', 0); ?>

                    <span>Updating every <strong><?php echo get_option('yotpo_plugin_time_update_count'); ?></strong> hours</span>

                    <div class="email_user_sl">
                        <span>Select another time</span>
                        <select id="cron-times_value">
                            <?php for ($i=0; $i <= 24; $i++) {
                                switch ($i) {
                                    case 0:
                                        $label = 'Never';
                                        break;
                                    case 1:
                                        $label = $i.' hour';
                                        break;
                                    default:
                                        $label = $i.' hours';
                                        break;
                                }
                                printf('<option value="%d" %s>%s</option>', $i, selected( $cron_hours, $i, 0 ), $label);
                            } ?>
                        </select>


                    </div>
                    <button id="add_update_time" class="block_user_add_bt_time">
                        Save
                    </button>
                </div>

            </div>
            <div>
<!---->
<!--              --><?php
//
//
//                  $total_reviews  = get_option('total_reviews');
//                  $average_score  = get_option('average_score');
//                  echo $total_reviews;
//                  echo '<br>';
//                  echo $average_score;
//              ?>
            </div>





    </div>

    <?php

}
