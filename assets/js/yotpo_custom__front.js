jQuery(document).ready(function($){

    function repeatXI(callback, interval, repeats, immediate) {
        var timer, trigger;
        trigger = function () {
            --repeats;
            if(callback() || !repeats){
                clearInterval(timer);
            }
        };

        interval = interval <= 0 ? 1000 : interval; // default: 1000ms
        repeats  = parseInt(repeats, 10) || 0;      // default: repeat forever
        timer    = setInterval(trigger, interval);

        if( !!immediate ) { // Coerce boolean
            trigger();
        }
    }

    $('.cg-wp-menu-wrapper .cg-announcements').css('display','none !important');

    var starsBlockFlag = true;

    $('.post-ratings.pal_fin').each(function () {
        if( starsBlockFlag ) {
            var startsBlock = $(this).detach();
            $('.wpb_text_column.wpb_content_element .wpb_wrapper > h1:first-of-type').after(startsBlock);
        }
        else {
            $(this).remove();
        }
        starsBlockFlag = false;
    });

    repeatXI(add_attr_q, 200, 10);
    function add_attr_q() {
        if( $('.yotpo-display-wrapper .text-m').length ) {
            $('.post-ratings.pal_fin.star-55 img#rating_66277_5').attr( "src", "/wp-content/plugins/wp-postratings/images/stars_crystal/rating_half.gif");
            $('.post-ratings.pal_fin.star-45 img#rating_66277_4').attr( "src", "/wp-content/plugins/wp-postratings/images/stars_crystal/rating_half.gif");
            $('.post-ratings.pal_fin.star-35 img#rating_66277_3').attr( "src", "/wp-content/plugins/wp-postratings/images/stars_crystal/rating_half.gif");
            $('.post-ratings.pal_fin.star-25 img#rating_66277_2').attr( "src", "/wp-content/plugins/wp-postratings/images/stars_crystal/rating_half.gif");
            $('.post-ratings.pal_fin.star-15 img#rating_66277_1').attr( "src", "/wp-content/plugins/wp-postratings/images/stars_crystal/rating_half.gif");
            return true;
        }
        return false;
    }

    repeatXI(remove_class_product, 200, 10);
    function remove_class_product() {
        if( $('.all_tabs_description_page .tab-description .col-lg-12.col-md-12').length ) {
            $('.all_tabs_description_page .tab-description .col-lg-12.col-md-12').addClass('tab_block_visual');
            $('.tab_block_visual').removeClass('col-lg-12');
            $('.tab_block_visual').removeClass('col-md-12');
            return true;
        }
        return false;
    }

    //есть ли табуляция на странице категорий
    repeatXI(add_attr_category, 200, 10);
    function add_attr_category() {
        if( !$('.all_bl_archive').length ) {
            $('.archive .col-lg-12.col-md-12').css("margin-top","0");
            return true;
        }
    }

    $( "body" ).on( "click", ".owl-prev", function(e) {
        e.preventDefault;

        $parent = $('.yotpo-owl-item').parent();
        $parent_v =$parent.prev().find('all_block_items');

        $parent_v.click();

    });

    repeatXI(add_attr_a, 200, 10);
    function add_attr_a(){
        var anchorReview = $('.yotpo-bottomline.pull-left.star-clickable .text-m');
        if( anchorReview.length ) {
            $('.verified-reviews_tab a').attr("href", "#tab-verified-reviews");
            anchorReview.attr("href", "#verified-reviews");
            return true;
        }
    }

    $(document).on('click', '.custom_rating_ct', function(){

        var elementClick = '.yotpo_widget_tab';

        $('ul.custom_tabs li').removeClass('active');
        $data_share = $('li.yotpo_widget_tab a').attr('data-tabs');

        $('li.yotpo_widget_tab').addClass('active');
        $('.all_tabs_description_page > div').slideUp();
        $('.tab-yotpo_widget').slideDown();

        var destination = $(elementClick).offset().top - 100;
        if ($.browser.safari) {
            $('body').animate({ scrollTop: destination }, 1100); //1100 - скорость
        } else {
            $('html').animate({ scrollTop: destination }, 1100);
        }
    });

    function loadReviews($this) {
        $data_name_category = $('.next_arrows_outer').attr('data-name-category');

        $('.owl-item div').css('color','#a2a2a2');
        $('.owl-item div').css('font-weight','normal');
        $all_rew_id = "";
        $t_reg = $this.attr( "data-rowspage");
        $all_rew_data_page='';
        $global_id = "";


        $('.yotpo-owl-item').removeClass('yotpo-owl-item');
        $this.addClass('yotpo-owl-item');


        $('.all_block_items__yotpo').each(function () {

            $all_rew_data_page += ',' + $(this).attr('data-page');
            $res_page = $all_rew_data_page.split(',');
            for($i=0; $i<=$res_page.length; $i++ ){
                if($t_reg == $res_page[$i]){
                    $div_none = ".all_block_items__yotpo" + $t_reg;
                    $(".all_block_items__yotpo").css('display','none');
                    $($div_none).css('display','block');
                    $global_id = "ok";
                }
            }

        });

        if($global_id != "ok"){
            $all_rew_id = "";
            $all_rew_data_page='';

            $extange = $('.all_block_items_yo').attr('data-exchange');


            $all_rew_id = $this.attr( "data-rowspage");
            $('.yotpo_reviews__ovner').each(function () {

                $all_rew_id += ',' + $(this).attr('data-id');
            });
            $div_none = ".all_block_items__yotpo" + $t_reg;


            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'download_new_review_items',
                    ids: $all_rew_id,
                    data_page: $t_reg,
                    data_name_category: $data_name_category,
                    extange: $extange
                },

                success: function(response){
                    $(".all_block_items__yotpo").css('display','none');
                    $($div_none).css('display','block');

                    $(".all_block_items_yo").append(response);


                    $('.all_block_items__yotpo').each(function () {

                        $all_rew_data_page += ',' + $(this).attr('data-page');
                        var $res_page = $all_rew_data_page.split(',');


                    });


                },
                error: function(a,b){
                    alert('Error');
                }
            })
        }

        var elementClick = '.all_block_items_yo';
        var destination = $(elementClick).offset().top - 100;
        if ($.browser.safari) {
            $('body').animate({ scrollTop: destination }, 1100); //1100 - скорость
        } else {
            $('html').animate({ scrollTop: destination }, 1100);
        }
    }

    $(document).on('click', '.all_block_items a', function(){

        loadReviews( $(this).parent() );
        // return false;

    });

    $(document).on('click', '.yotpobuttonlabel_addcategory', function(){

        $('.yotpo_name_review').val('');
        $('.yotpo_title_review').val('');
        $('.textareaform_review').val('');
        $('.post-ratings.pal_fin_category img').attr('src','/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif');

        $('#yotpo_form_review').slideDown();

    });

    $(document).on('click', '.yotpobuttonlabel_adcat_null', function(){

        $('.yotpo_name_review').val('');
        $('.yotpo_title_review').val('');
        $('.textareaform_review').val('');
        $('.post-ratings.pal_fin_category img').attr('src','/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif');

        $('#yotpo_form_review').slideDown();

    });


    $(document).on('click', 'span.yotpo-icon.yotpo-icon-cross', function(){
        $('.yotpo-navrating_category_answer').remove();
    });



    //категории клик
    $(document).on('click', '.all_block_items_category a', function(e){

        loadReviews_category( $(this).parent() );

    });


    $( ".home" ).on( "click", ".post-ratings img", function() {
        $star_r = $(this).attr('data-star');

        $.ajax({
            url: ajaxurl,
            method: 'POST',
            dataType: 'json',
            data: {
                action: 'home_page_star_rating',
                home_rating: $star_r
            },
            success: function(response){
                $(".wpb_wrapper .post-ratings").remove();
                $(' .star_custom_icons').css('display','none');
                $(".wpb_text_column.wpb_content_element .wpb_wrapper h1:first").after(response);
            },
            error: function(a,b){
                alert('Error');
            }
        })
    });


    $( "body" ).on( "click", "ul.custom_tabs a", function() {
        $('.col-lg-12.col-md-12').css('display','none');
    });

    $( "body" ).on( "click", "li.description_tab a", function() {
        $('.col-lg-12.col-md-12').css('display','block');
    });

    $( "body" ).on( "click", "ul.custom_tabs li", function() {
        $('ul.custom_tabs li').removeClass('active');
        $data_share = $(this).attr('data-tabs');

        $(this).addClass('active');
        $('.all_tabs_description_page > div').slideUp();
        $('.' + $data_share).show();
    });

    $( "body" ).on( "click", "li.yotpo_widget_tab a", function() {
        $('ul.custom_tabs li').removeClass('active');
        $data_share = $(this).attr('data-tabs');

        $(this).addClass('active');
        $('.all_tabs_description_page > div').slideUp();
        $('.' + $data_share).slideDown();
    });

    $( "body" ).on( "click", ".verified-reviews_tab a", function() {

        $('ul.custom_tabs li').removeClass('active');

        $(this).addClass('active');
        $('.wc-tab').slideUp();
        $('div#tab-verified-reviews').css('display','block !important');
    });

    //для определения лайка
    $( "body" ).on( "click", ".helpful_div_bt_lilke", function() {
        $diving = $(this);
        $dt_parent = $diving.parents('.helpful_div_bt_res');
        $dataanswer = $diving.find( 'span' ).attr('data-id_answer');

        if(localStorage.getItem('yotpo_settings_like_' + $dataanswer) === null){
            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'helpful_div_bt_res__all',
                    dataanswer: $dataanswer, //id  товара
                    add_like: 'true', //увеличить лайк
                    reduce_like: 'false', //уменьшить лайк
                    add_dislike: 'false', //увеличить дизлайк
                    reduce_dislike: 'false'  //уменьшить дизлайк
                },
                success: function(response){
                    $dt_parent.html(response);

                    // $dataanswer = $diving.find( '.counts_thumbs_up' ).text(response);
                },
                error: function(a,b){
                    alert('Error');
                }
            })

            localStorage.setItem('yotpo_settings_like_' + $dataanswer, 'true');
            localStorage.setItem('yotpo_settings_dislike_' + $dataanswer, 'false');

        } //если не существует мы увеличиваем лайк.
        else if(localStorage.getItem('yotpo_settings_like_' + $dataanswer) === 'true'){
            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'helpful_div_bt_res__all',
                    dataanswer: $dataanswer, //id  товара
                    add_like: 'false', //увеличить лайк
                    reduce_like: 'true', //уменьшить лайк
                    add_dislike: 'false', //увеличить дизлайк
                    reduce_dislike: 'false'  //уменьшить дизлайк
                },
                success: function(response){
                    $dt_parent.html(response);

                    // $dataanswer = $diving.find( '.counts_thumbs_up' ).text(response);
                },
                error: function(a,b){
                    alert('Error');
                }
            })
            //удаляем ключи
            localStorage.removeItem('yotpo_settings_like_' + $dataanswer);
            localStorage.removeItem('yotpo_settings_dislike_' + $dataanswer);

        } // если он уже лайкнут, то только уменьшаем лайк
        else if(localStorage.getItem('yotpo_settings_like_' + $dataanswer) === 'false'){
            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'helpful_div_bt_res__all',
                    dataanswer: $dataanswer, //id  товара
                    add_like: 'true', //увеличить лайк
                    reduce_like: 'false', //уменьшить лайк
                    add_dislike: 'false', //увеличить дизлайк
                    reduce_dislike: 'true'  //уменьшить дизлайк
                },
                success: function(response){
                    $dt_parent.html(response);

                    // $dataanswer = $diving.find( '.counts_thumbs_up' ).text(response);
                },
                error: function(a,b){
                    alert('Error');
                }
            })
            localStorage.setItem('yotpo_settings_like_' + $dataanswer, 'true');
            localStorage.setItem('yotpo_settings_dislike_' + $dataanswer, 'false');
        } //если этот отзыв до этого дизлайкнут, то есть лайк false, то минус 1дизлайк и плюс 1лайк


    });


    //для определения дизлайка
    $( "body" ).on( "click", ".helpful_div_bt_res_dislike", function() {
        $diving = $(this);

        $dt_parent = $diving.parents('.helpful_div_bt_res');
        $dataanswer = $diving.find( 'span' ).attr('data-id_answer');

             if(localStorage.getItem('yotpo_settings_dislike_' + $dataanswer) === null) {
                $.ajax({
                    url: ajaxurl,
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        action: 'helpful_div_bt_res__all',
                        dataanswer: $dataanswer, //id  товара
                        add_like: 'false', //увеличить лайк
                        reduce_like: 'false', //уменьшить лайк
                        add_dislike: 'true', //увеличить дизлайк
                        reduce_dislike: 'false'  //уменьшить дизлайк
                    },
                    success: function(response){
                        $dt_parent.html(response);
                    },
                    error: function(a,b){
                        alert('Error');
                    }
                })

                localStorage.setItem('yotpo_settings_dislike_' + $dataanswer, 'true');

        } //если не существует мы увеличиваем дизлайк.
        else if(localStorage.getItem('yotpo_settings_dislike_' + $dataanswer) === 'true'){
            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'helpful_div_bt_res__all',
                    dataanswer: $dataanswer, //id  товара
                    add_like: 'false', //увеличить лайк
                    reduce_like: 'false', //уменьшить лайк
                    add_dislike: 'false', //увеличить дизлайк
                    reduce_dislike: 'true'  //уменьшить дизлайк
                },
                success: function(response){
                    $dt_parent.html(response);

                    // $dataanswer = $diving.find( '.counts_thumbs_up' ).text(response);
                },
                error: function(a,b){
                    alert('Error');
                }
            })
            //удаляем ключи
            localStorage.removeItem('yotpo_settings_like_' + $dataanswer);
            localStorage.removeItem('yotpo_settings_dislike_' + $dataanswer);

        } // если он уже дизлайкнут, то только уменьшаем дизлайк
        else if(localStorage.getItem('yotpo_settings_dislike_' + $dataanswer) === 'false'){
            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'helpful_div_bt_res__all',
                    dataanswer: $dataanswer, //id  товара
                    add_like: 'false', //увеличить лайк
                    reduce_like: 'true', //уменьшить лайк
                    add_dislike: 'true', //увеличить дизлайк
                    reduce_dislike: 'false'  //уменьшить дизлайк
                },
                success: function(response){
                    $dt_parent.html(response);

                    // $dataanswer = $diving.find( '.counts_thumbs_up' ).text(response);
                },
                error: function(a,b){
                    alert('Error');
                }
            })
            localStorage.setItem('yotpo_settings_dislike_' + $dataanswer, 'true');
            localStorage.setItem('yotpo_settings_like_' + $dataanswer, 'false');
        } //если этот отзыв до этого лайкнут, то есть лайк true, то минус 1лайк и плюс 1дизлайк
    });

    $( "body" ).on( "click", ".socisl_bbt_spanshare", function() {
        $data_share = $(this).attr('data_share');
        $(' .div_'+ $data_share).addClass("socisl_bbt_spanshare_add");
        $(' .socisl_bbt_spanshare_add').css('visibility','visible');
    });

    //подсвечивает description на странице категории
    repeatXI(add_attr_tab, 200, 10);
    function add_attr_tab() {
        if( $('li.description_tab').length ) {
            if(!window.location.hash && window.location.hash.indexOf("#yotpo-page-") === -1) {
                $('li.description_tab').addClass("active");
                return true;
            }
        }
    }


    //клик на стрелки категории
    $(document).on('click', '.next_arrows_outer.yotpo_category_arrows .owl-next', function (e) {
        e.preventDefault();
        // alert("GOOGOGOGOGO");
        var currentItem = $('.yotpo-owl-item');
        currentItem.removeClass('yotpo-owl-item');
        var nextParent;
        if( currentItem.parent().is( ":last-child" ) ) {
            nextParent = $('.next_arrows_outer.yotpo_category_arrows .owl-item:first-child')
        }
        else {
            nextParent = currentItem.parent().next();
        }
        var nextItem = nextParent.children();
        nextItem.addClass('yotpo-owl-item');
        updateHashInUrl(nextItem);
        loadReviews_category(nextItem);
    });

    $(document).on('click', '.next_arrows_outer.yotpo_category_arrows .owl-prev', function (e) {
        e.preventDefault();
        var currentItem = $('.yotpo-owl-item');
        currentItem.removeClass('yotpo-owl-item');
        var prevParent;
        if( currentItem.parent().is( ":first-child" ) ) {
            prevParent = $('.next_arrows_outer.yotpo_category_arrows .owl-item:last-child')
        }
        else {
            prevParent = currentItem.parent().prev();
        }
        var prevItem = prevParent.children();
        prevItem.addClass('yotpo-owl-item');
        updateHashInUrl(prevItem);
        loadReviews_category(prevItem);
    });

    $(document).on('click', '.next_arrows_outer .owl-next', function () {
        if(!$('.next_arrows_outer').hasClass('yotpo_category_arrows')) {
            var currentItem = $('.yotpo-owl-item');
            currentItem.removeClass('yotpo-owl-item');
            var nextParent;
            if (currentItem.parent().is(":last-child")) {
                nextParent = $('.next_arrows_outer .owl-item:first-child')
            }
            else {
                nextParent = currentItem.parent().next();
            }
            var nextItem = nextParent.children();
            nextItem.addClass('yotpo-owl-item');
            updateHashInUrl(nextItem);
            loadReviews(nextItem);
        }
    });

    $(document).on('click', '.next_arrows_outer .owl-prev', function () {
        if(!$('.next_arrows_outer').hasClass('yotpo_category_arrows')) {
            var currentItem = $('.yotpo-owl-item');
            currentItem.removeClass('yotpo-owl-item');
            var prevParent;
            if( currentItem.parent().is( ":first-child" ) ) {
                prevParent = $('.next_arrows_outer .owl-item:last-child')
            }
            else {
                prevParent = currentItem.parent().prev();
            }
            var prevItem = prevParent.children();
            prevItem.addClass('yotpo-owl-item');
            updateHashInUrl(prevItem);
            loadReviews(prevItem);
        }

    });

    function updateHashInUrl($item) {
        if(history.pushState) {
            history.pushState(null, null, '#' + $item.attr('data-hash'));
        }
        else {
            location.hash = '#' + $item.attr('data-hash');
        }
    }

    jQuery( "span.yotpo-icon.yotpo-icon-circle-checkmark.yotpo-action-hover" ).mouseover(function() {
        $this = jQuery(this).parent().next();

        $yotpo_tool = $this.find('.yotpo-tool-tip');
        $yotpo_tool.show();
        $yotpo_tool.css("opacity","1");

    }).mouseout(function() {
        $this = jQuery(this).parent().next();

        $yotpo_tool = $this.find('.yotpo-tool-tip');
        $yotpo_tool.hide();
        $yotpo_tool.css("opacity","0");
    });

     $(document).on('click', '.post-ratings.pal_fin_category img', function () {

        $('.post-ratings img').attr("src","/wp-content/plugins/wp-postratings/images/stars_crystal/rating_on.gif");

        $(this).attr("src","/wp-content/plugins/wp-postratings/images/stars_crystal/rating_over.gif");
        $current_star_id = $(this).attr('id');
        $get_id = $(this).attr('id');

        $current_item =  $(this);
        do{
            $current_item.attr("src","/wp-content/plugins/wp-postratings/images/stars_crystal/rating_over.gif");
            $current_item = $current_item.prev();
        }while ($current_item.length);

         $('#yotpo_form_review').attr("data-score", $get_id);

     });

    $(document).on('click', 'a.text-m', function () {

        $reviz = $('.text-m').attr("href");

        if($reviz !== undefined){

            var yotpo_widget_tab = $('.yotpo_widget_tab').find('a'); 
            yotpo_widget_tab.click();

            var elementClick = '.woocommerce-tabs';
            var destination = $(elementClick).offset().top - 100;
            if ($.browser.safari) {
                $('body').animate({ scrollTop: destination }, 1100); //1100 - скорость
            } else {
                $('html').animate({ scrollTop: destination }, 1100);
            }
        }

    });

    //отзывы категории
    $(document).on('click', '.form_review_button', function () {

        var $yotpo_form_review = $('#yotpo_form_review'),
            score_category = $yotpo_form_review.attr("data-score"),
            $yotpo_name_review = $yotpo_form_review.find('.yotpo_name_review'),
            yotpo_name_review_val = $yotpo_name_review.val(),
            $yotpo_title_review = $yotpo_form_review.find('.yotpo_title_review'),
            yotpo_title_review_val = $yotpo_title_review.val(),
            $textareaform_review = $yotpo_form_review.find('.textareaform_review'),
            textareaform_review_val = $textareaform_review.val(),
            rew_category_name = $yotpo_form_review.data("category_name"),
            rew_category_id = $yotpo_form_review.data("category_id");
        if(
            yotpo_name_review_val != '' &&
            yotpo_title_review_val != '' &&
            textareaform_review_val != '' &&
            score_category != ''
        ){
            var currentdate = new Date();
            var datetime =
                currentdate.getFullYear()  + "-"
                + (currentdate.getMonth()+1) + "-"
                + currentdate.getDate() + " "
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds();

            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'form_review_button_funk',
                    score_category: score_category,
                    yotpo_name_review: yotpo_name_review_val,
                    yotpo_title_review: yotpo_title_review_val,
                    textareaform_review: textareaform_review_val,
                    rew_category_name: rew_category_name,
                    rew_category_id: rew_category_id,
                    datetime: datetime
                },
                success: function(response){

                    $yotpo_form_review.slideUp();

                    $('.yotpo-navrating_category_answer').html('<div class="yotpo-messages"> <div class="yotpo-thank-you" data-type="share"> <div class="yotpo-icon-btn transparent-color-btn"><span class="yotpo-icon yotpo-icon-cross"></span></div> <div class="yotpo-thankyou-header text-3xl"> <span class="yotpo-icon yotpo-icon-heart"></span> <span class="title-thank-span">Thank you for posting a review!</span> </div>  <div class="yotpo-thankyou-content"> <span>We value your input. Share your review so everyone else can enjoy it too.</span> </div>   </div>  </div>');
                },
                error: function(a,b){
                    alert('Error');
                }
            })
        } else {

            $yotpo_name_review.css('border-color','#ccc');
            $yotpo_title_review.css('border-color','#ccc');
            $textareaform_review.css('border-color','#ccc');

            var $number_of_stars_block = $('.pal_fin_category>span');
            $number_of_stars_block.css('border-bottom','transparent');

            if(yotpo_name_review_val == ''){
                $yotpo_name_review.css('border-color','red');
            }
            if(yotpo_title_review_val == ''){
                $yotpo_title_review.css('border-color','red');
            }
            if(textareaform_review_val == ''){
                $textareaform_review.css('border-color','red');
            }
            if(score_category == ''){
                $number_of_stars_block.css('border-bottom','1px solid red');
            }

        }

    });
    //для определения лайка категории
    $( "body" ).on( "click", ".all_customs_category .helpful_div_bt_res .helpful_div_bt_lilke", function() {
        $diving = $(this);
        $dt_parent = $diving.parents('.all_customs_category .helpful_div_bt_res');
        $dataanswer = $diving.find( 'span' ).attr('data-id_answer');
        if(localStorage.getItem('yotpo_category_like_' + $dataanswer) === null){

            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'helpful_div_bt_res__category',
                    dataanswer: $dataanswer, //id  товара
                    add_like: 'true', //увеличить лайк
                    reduce_like: 'false', //уменьшить лайк
                    add_dislike: 'false', //увеличить дизлайк
                    reduce_dislike: 'false'  //уменьшить дизлайк
                },
                success: function(response){
                    $dt_parent.html(response);

                    // $dataanswer = $diving.find( '.counts_thumbs_up' ).text(response);
                },
                error: function(a,b){
                    alert('Error');
                }
            })

            localStorage.setItem('yotpo_category_like_' + $dataanswer, 'true');
            localStorage.setItem('yotpo_category_dislike_' + $dataanswer, 'false');

        } //если не существует мы увеличиваем лайк.
        else if(localStorage.getItem('yotpo_category_like_' + $dataanswer) === 'true'){

            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'helpful_div_bt_res__category',
                    dataanswer: $dataanswer, //id  товара
                    add_like: 'false', //увеличить лайк
                    reduce_like: 'true', //уменьшить лайк
                    add_dislike: 'false', //увеличить дизлайк
                    reduce_dislike: 'false'  //уменьшить дизлайк
                },
                success: function(response){
                    $dt_parent.html(response);

                    // $dataanswer = $diving.find( '.counts_thumbs_up' ).text(response);
                },
                error: function(a,b){
                    alert('Error');
                }
            })
            //удаляем ключи
            localStorage.removeItem('yotpo_category_like_' + $dataanswer);
            localStorage.removeItem('yotpo_category_dislike_' + $dataanswer);

        } // если он уже лайкнут, то только уменьшаем лайк
        else if(localStorage.getItem('yotpo_category_like_' + $dataanswer) === 'false'){

            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'helpful_div_bt_res__category',
                    dataanswer: $dataanswer, //id  товара
                    add_like: 'true', //увеличить лайк
                    reduce_like: 'false', //уменьшить лайк
                    add_dislike: 'false', //увеличить дизлайк
                    reduce_dislike: 'true'  //уменьшить дизлайк
                },
                success: function(response){
                    $dt_parent.html(response);

                    // $dataanswer = $diving.find( '.counts_thumbs_up' ).text(response);
                },
                error: function(a,b){
                    alert('Error');
                }
            })
            localStorage.setItem('yotpo_category_like_' + $dataanswer, 'true');
            localStorage.setItem('yotpo_category_dislike_' + $dataanswer, 'false');
        } //если этот отзыв до этого дизлайкнут, то есть лайк false, то минус 1дизлайк и плюс 1лайк


    });


    //для определения дизлайка категории
    $( "body" ).on( "click", ".all_customs_category .helpful_div_bt_res .helpful_div_bt_res_dislike", function() {
        $diving = $(this);

        $dt_parent = $diving.parents('.all_customs_category .helpful_div_bt_res');
        $dataanswer = $diving.find( 'span' ).attr('data-id_answer');

        if(localStorage.getItem('yotpo_category_dislike_' + $dataanswer) === null) {
            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'helpful_div_bt_res__category',
                    dataanswer: $dataanswer, //id  товара
                    add_like: 'false', //увеличить лайк
                    reduce_like: 'false', //уменьшить лайк
                    add_dislike: 'true', //увеличить дизлайк
                    reduce_dislike: 'false'  //уменьшить дизлайк
                },
                success: function(response){
                    $dt_parent.html(response);

                    // $dataanswer = $diving.find( '.counts_thumbs_up' ).text(response);
                },
                error: function(a,b){
                    alert('Error');
                }
            })

            localStorage.setItem('yotpo_category_dislike_' + $dataanswer, 'true');

        } //если не существует мы увеличиваем дизлайк.
        else if(localStorage.getItem('yotpo_category_dislike_' + $dataanswer) === 'true'){
            alert("localStorage.getItem('yotpo_category_dislike_' + $dataanswer) === true");
            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'helpful_div_bt_res__category',
                    dataanswer: $dataanswer, //id  товара
                    add_like: 'false', //увеличить лайк
                    reduce_like: 'false', //уменьшить лайк
                    add_dislike: 'false', //увеличить дизлайк
                    reduce_dislike: 'true'  //уменьшить дизлайк
                },
                success: function(response){
                    $dt_parent.html(response);

                    // $dataanswer = $diving.find( '.counts_thumbs_up' ).text(response);
                },
                error: function(a,b){
                    alert('Error');
                }
            })
            //удаляем ключи
            localStorage.removeItem('yotpo_category_like_' + $dataanswer);
            localStorage.removeItem('yotpo_category_dislike_' + $dataanswer);

        } // если он уже дизлайкнут, то только уменьшаем дизлайк
        else if(localStorage.getItem('yotpo_category_dislike_' + $dataanswer) === 'false'){
            alert("localStorage.getItem('yotpo_category_dislike_' + $dataanswer) === false");
            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'helpful_div_bt_res__category',
                    dataanswer: $dataanswer, //id  товара
                    add_like: 'false', //увеличить лайк
                    reduce_like: 'true', //уменьшить лайк
                    add_dislike: 'true', //увеличить дизлайк
                    reduce_dislike: 'false'  //уменьшить дизлайк
                },
                success: function(response){
                    $dt_parent.html(response);

                    // $dataanswer = $diving.find( '.counts_thumbs_up' ).text(response);
                },
                error: function(a,b){
                    alert('Error');
                }
            })
            localStorage.setItem('yotpo_category_dislike_' + $dataanswer, 'true');
            localStorage.setItem('yotpo_category_like_' + $dataanswer, 'false');
        } //если этот отзыв до этого лайкнут, то есть лайк true, то минус 1лайк и плюс 1дизлайк

    });

    //загрузка отзывов категорий
    function loadReviews_category($this) {
        $data_name_category = $('.next_arrows_outer').attr('data-name-category');

        $('.owl-item div').css('color','#a2a2a2');
        $('.owl-item div').css('font-weight','normal');
        $all_rew_id = "";
        $t_reg = $this.attr( "data-rowspage");
        $all_rew_data_page='';
        $global_id = "";

        $('.yotpo-owl-item').removeClass('yotpo-owl-item');
        $this.addClass('yotpo-owl-item');

        $('.all_block_items__yotpo_category').each(function () {

            if( $all_rew_data_page !== '' ) {
                $all_rew_data_page += ',';
            }
            $all_rew_data_page += $(this).attr('data-page');
        });

        $res_page = $all_rew_data_page.split(',');
        for($i=0; $i<=$res_page.length; $i++ ){
            if($t_reg == $res_page[$i]){
                $div_none = ".all_block_items__yotpo_category.all_block_items__yotpo" + $t_reg;

                $(".all_block_items__yotpo_category").css('display','none');
                $($div_none).css('display','block');
                $global_id = "ok";
            }
        }

        if($global_id != "ok"){
            $all_rew_id = "";
            $all_rew_data_page='';

            $all_rew_id = $this.attr( "data-rowspage");
            $('.yotpo_reviews__ovner_category').each(function () {

                $all_rew_id += ',' + $(this).attr('data-id');
            });
            $div_none = ".all_block_items__yotpo_category" + $t_reg;

            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'download_new_review_items_category',
                    ids: $all_rew_id,
                    data_page: $t_reg,
                    data_name_category: $data_name_category
                },

                success: function(response){
                    $(".all_block_items__yotpo_category").css('display','none');
                    $($div_none).css('display','block');

                    $(".all_block_items_yo").append(response);


                    $('.all_block_items__yotpo_category').each(function () {

                        $all_rew_data_page += ',' + $(this).attr('data-page');
                        var $res_page = $all_rew_data_page.split(',');
                    });
                },
                error: function(a,b){
                    alert('Error');
                }
            })
        }

        var elementClick = '.all_block_items_yo';
        var destination = $(elementClick).offset().top - 100;
        if ($.browser.safari) {
            $('body').animate({ scrollTop: destination }, 1100); //1100 - скорость
        } else {
            $('html').animate({ scrollTop: destination }, 1100);
        }
    }

    var url = window.location.href;

    if( $('.all_block_items_category').length ) {
        var itemsCount = $('.all_block_items_category').length;
    }
    else {
        var itemsCount = $('.all_block_items').length;
    }

    function owlCarouselAfterInit() {
        // проверка на то существует ли хеш в урле
        if(window.location.hash) {
            // проверка это хеш для отзывов или нет
            if( window.location.hash.indexOf("#yotpo-page-") !== -1 ) {
                var pageNumber = window.location.hash.replace("#yotpo-page-", "");

                var item = $('.yotpo_widget_tab[data-tabs="tab-yotpo_widget"]');
                $data_share = item.attr('data-tabs');
                $('ul.custom_tabs li.active').removeClass('active');
                item.addClass('active');
                $('.all_tabs_description_page > div').hide();
                $('.' + $data_share).show();

                if( $('.all_block_items__yotpo_category').length ) {
                    loadReviews_category($('.all_block_items_category[data-hash="yotpo-page-' + pageNumber + '"]'));
                }
                else {
                    loadReviews($('.all_block_items[data-hash="yotpo-page-' + pageNumber + '"]'));
                }
            }
        }
        else {
            // сделать первый элемент активным
            $(".all_block_items[data-hash='yotpo-page-1']").addClass('yotpo-owl-item');
            $(".all_block_items_category[data-hash='yotpo-page-1']").addClass('yotpo-owl-item');
        }
    }

    var startPosition = 0;
    // проверка на то существует ли хеш в урле
    if(window.location.hash) {
        // проверка это хеш для отзывов или нет
        if (window.location.hash.indexOf("#yotpo-page-") !== -1) {
            var pageNumber = window.location.hash.replace("#yotpo-page-", "");
            startPosition = pageNumber-1;
        }
    }

    if( (url.indexOf('wp-admin')) == '-1' && $('.next_arrows').length) {
        var $owl = $('.next_arrows');
        $owl.on('initialized.owl.carousel', owlCarouselAfterInit);
        $owl.on('dragged.owl.carousel', function(property){
            var hash = window.location.hash.replace('#', '');
            if( $('.all_block_items__yotpo_category').length ) {
                loadReviews_category($('.all_block_items_category[data-hash="' + hash + '"]'));
            }
            else {
                loadReviews($('.all_block_items[data-hash="' + hash + '"]'));
            }
        });

        if( itemsCount < 9 ) {
            $width = (28.556 * itemsCount) ;
            $('.next_arrows_outer').css('max-width', $width)
            $owl.owlCarouselNew({
                margin:7,
                nav: true,
                navigationText : ["",""],
                URLhashListener: true,
                items:itemsCount,
                startPosition: startPosition,
                animateIn: true,
                // center: true,
                afterInit: owlCarouselAfterInit
            });

        }else{
            $owl.owlCarouselNew({
                margin:7,
                nav: true,
                navigationText : ["",""],
                URLhashListener: true,
                items:9,
                startPosition: startPosition,
                animateIn: true,
                afterInit: owlCarouselAfterInit
            });
        }

        $owl.trigger('refresh.owl.carousel');
        if( itemsCount == '1' ) {
            $('.next_arrows_outer.yotpo_category_arrows').remove();
        }
        if( itemsCount > 1 ) {
            $('.next_arrows_outer .owl-carousel .owl-nav').show();
        }
    }
});
